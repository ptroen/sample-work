
var map;
var markers = [];
var initialLocation;

var geolocation = new google.maps.LatLng(49.282018, -123.107659);
var browserSupportFlag =  new Boolean();

function initialize() {
	// google.maps.MapTypeId.TERRAIN looks better imho
	// google.maps.MapTypeId.ROADMAP   
	// put this as a option later on
	var gastown = new google.maps.LatLng(49.282018, -123.107659);
	var mapOptions = {
		zoom: 18,
		center: gastown,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	map = new google.maps.Map(document.getElementById('map-canvas'),
	mapOptions);
        

	// Try W3C Geolocation (Preferred)
	if(navigator.geolocation) {
	  browserSupportFlag = true;
	  navigator.geolocation.getCurrentPosition(function(position) {
	    initialLocation = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
	    map.setCenter(initialLocation);
	  }, function() {
	    handleNoGeolocation(browserSupportFlag);
	  });
	}
	// Browser doesn't support Geolocation
	else {
	  browserSupportFlag = false;
	  handleNoGeolocation(browserSupportFlag);
	}

	getData();

	google.maps.event.addListener(map, 'center_changed', function() {
    	// 3 seconds after the center of the map has changed, get the new data
     	getData();
  	});
}

// Add a marker to the map and push to the array.
function addMarker(location,url) {

	var infowindow = new google.maps.InfoWindow({
             content: url
    });

	marker = new google.maps.Marker({
		position: location,
		map: map,
		title:""
	});

	markers.push(marker);
	google.maps.event.addListener(marker, 'click', function() {
		infowindow.open(map,marker);
		infowindow.setContent(this.html);
		infowindow.open(map, this);
	});
}
       
function handleNoGeolocation(errorFlag) {
	if (errorFlag == true) {
		alert("Geolocation service failed.");
		initialLocation = playerize;
	} else {
		alert("Your browser doesn't support geolocation. We've placed you in Siberia.");
		initialLocation = playerize;
	}
	map.setCenter(initialLocation);
} 


// Sets the map on all markers in the array.
function setAllMap(map) {
	for (var i = 0; i < markers.length; i++) {
		markers[i].setMap(map);
	}
}

// Removes the overlays from the map, but keeps them in the array.
function clearOverlays() {
	setAllMap(null);
}

// Shows any overlays currently in the array.
function showOverlays() {
	setAllMap(map);
}

// Deletes all markers in the array by removing references to them.
function deleteOverlays() {
	clearOverlays();
	markers = [];
}

google.maps.event.addDomListener(window, 'load', initialize);