!ifndef _WriteEnvStr_nsh
!define _WriteEnvStr_nsh
 
!include WinMessages.nsh
 
!ifndef WriteEnvStr_RegKey
  !ifdef ALL_USERS
    !define WriteEnvStr_RegKey \
       'HKLM "SYSTEM\CurrentControlSet\Control\Session Manager\Environment"'
  !else
    !define WriteEnvStr_RegKey 'HKCU "Environment"'
  !endif
!endif
 
Function openLinkNewWindow
  Push $3
  Exch
  Push $2
  Exch
  Push $1
  Exch
  Push $0
  Exch
 
  ReadRegStr $0 HKCR "http\shell\open\command" ""
# Get browser path
    DetailPrint $0
  StrCpy $2 '"'
  StrCpy $1 $0 1
  StrCmp $1 $2 +2 # if path is not enclosed in " look for space as final char
    StrCpy $2 ' '
  StrCpy $3 1
  loop:
    StrCpy $1 $0 1 $3
    DetailPrint $1
    StrCmp $1 $2 found
    StrCmp $1 "" found
    IntOp $3 $3 + 1
    Goto loop
 
  found:
    StrCpy $1 $0 $3
    StrCmp $2 " " +2
      StrCpy $1 '$1"'
 
  Pop $0
  Exec '$1 $0'
  Pop $0
  Pop $1
  Pop $2
  Pop $3
FunctionEnd
 
!macro _OpenURL URL
Push "${URL}"
Call openLinkNewWindow
!macroend
 
!define OpenURL '!insertmacro "_OpenURL"'



#
# WriteEnvStr - Writes an environment variable
# Note: Win9x systems requires reboot
#
# Example:
#  Push "HOMEDIR"           # name
#  Push "C:\New Home Dir\"  # value
#  Call WriteEnvStr
#
Function WriteEnvStr
  Exch $1 ; $1 has environment variable value
  Exch
  Exch $0 ; $0 has environment variable name
  Push $2
 
  Call IsNT
  Pop $2
  StrCmp $2 1 WriteEnvStr_NT
    ; Not on NT
    StrCpy $2 $WINDIR 2 ; Copy drive of windows (c:)
    FileOpen $2 "$2\autoexec.bat" a
    FileSeek $2 0 END
    FileWrite $2 "$\r$\nSET $0=$1$\r$\n"
    FileClose $2
    SetRebootFlag true
    Goto WriteEnvStr_done

  WriteEnvStr_NT:
      WriteRegExpandStr ${WriteEnvStr_RegKey} $0 $1
      SendMessage ${HWND_BROADCAST} ${WM_WININICHANGE} \
        0 "STR:Environment" /TIMEOUT=5000
 
  WriteEnvStr_done:
    Pop $2
    Pop $0
    Pop $1
FunctionEnd
 
#
# un.DeleteEnvStr - Removes an environment variable
# Note: Win9x systems requires reboot
#
# Example:
#  Push "HOMEDIR"           # name
#  Call un.DeleteEnvStr
#
Function un.DeleteEnvStr
  Exch $0 ; $0 now has the name of the variable
  Push $1
  Push $2
  Push $3
  Push $4
  Push $5
 
  Call un.IsNT
  Pop $1
  StrCmp $1 1 DeleteEnvStr_NT
    ; Not on NT
    StrCpy $1 $WINDIR 2
    FileOpen $1 "$1\autoexec.bat" r
    GetTempFileName $4
    FileOpen $2 $4 w
    StrCpy $0 "SET $0="
    SetRebootFlag true
 
    DeleteEnvStr_dosLoop:
      FileRead $1 $3
      StrLen $5 $0
      StrCpy $5 $3 $5
      StrCmp $5 $0 DeleteEnvStr_dosLoop
      StrCmp $5 "" DeleteEnvStr_dosLoopEnd
      FileWrite $2 $3
      Goto DeleteEnvStr_dosLoop
 
    DeleteEnvStr_dosLoopEnd:
      FileClose $2
      FileClose $1
      StrCpy $1 $WINDIR 2
      Delete "$1\autoexec.bat"
      CopyFiles /SILENT $4 "$1\autoexec.bat"
      Delete $4
      Goto DeleteEnvStr_done
 
  DeleteEnvStr_NT:
    DeleteRegValue ${WriteEnvStr_RegKey} $0
    SendMessage ${HWND_BROADCAST} ${WM_WININICHANGE} \
      0 "STR:Environment" /TIMEOUT=5000
 
  DeleteEnvStr_done:
    Pop $5
    Pop $4
    Pop $3
    Pop $2
    Pop $1
    Pop $0
FunctionEnd

!ifndef IsNT_KiCHiK
!define IsNT_KiCHiK
 
#
# [un.]IsNT - Pushes 1 if running on NT, 0 if not
#
# Example:
#   Call IsNT
#   Pop $0
#   StrCmp $0 1 +3
#     MessageBox MB_OK "Not running on NT!"
#     Goto +2
#     MessageBox MB_OK "Running on NT!"
#
!macro IsNT UN
Function ${UN}IsNT
  Push $0
  ReadRegStr $0 HKLM \
    "SOFTWARE\Microsoft\Windows NT\CurrentVersion" CurrentVersion
  StrCmp $0 "" 0 IsNT_yes
  ; we are not NT.
  Pop $0
  Push 0
  Return
 
  IsNT_yes:
    ; NT!!!
    Pop $0
    Push 1
FunctionEnd
!macroend
!insertmacro IsNT ""
!insertmacro IsNT "un."
 
!endif ; IsNT_KiCHiK

!endif ; _WriteEnvStr_nsh


; UserVars.nsi
;
; This script shows you how to declare and user variables.

;--------------------------------

  Name "Stars And Dynasties"
  OutFile "StarsAndDynastiesInstaller.exe"
  
  InstallDir "$PROGRAMFILES\StarsAndDynasties\"
  
  RequestExecutionLevel admin

;--------------------------------

  ;Pages
  Page directory
  Page instfiles

  UninstPage uninstConfirm
  UninstPage instfiles

;--------------------------------
; Declaration of user variables (Var command), allowed charaters for variables names : [a-z][A-Z][0-9] and '_'

  Var "Name"
  Var "Serial"
  Var "Info"

;--------------------------------
; Installer
!include nsDialogs.nsh
!include LogicLib.nsh
XPStyle on


Var Dialog
Var Label
Var Text
Var Dialog2
Var Label2
Var Text2
Var Dialog3
Var Label3
Var Text3
Var Dialog4
Var Label4
Var Text4

Page custom nsDialogsPageMercudialBrowser nsDialogsPageMercudialBrowserLeave
Page instfiles
Page custom nsDialogsPageMercudialCloneBrowser nsDialogsPageMercudialCloneLeave
Page instfiles


Function nsDialogsPageMercudialBrowser

	nsDialogs::Create 1018
	Pop $Dialog3

	${If} $Dialog3 == error
		Abort
	${EndIf}

	${NSD_CreateLabel} 0 0 100% 12u "Click Next to open a link of Tortoise hg. Please download and Install It!"
	Pop $Label3
	
	; add Mercudial download here
	${OpenURL} "http://tortoisehg.bitbucket.org/download/index.html"


	nsDialogs::Show

FunctionEnd

Function nsDialogsPageMercudialBrowserLeave

FunctionEnd

Function nsDialogsPageMercudialCloneBrowser

	nsDialogs::Create 1018
	Pop $Dialog4

	${If} $Dialog4 == error
		Abort
	${EndIf}

	${NSD_CreateLabel} 0 0 100% 12u "Click Install once you installed Tortoisehg. This will begin copying the files over."
	Pop $Label4


	nsDialogs::Show

FunctionEnd

Function Done

   FileOpen $9 $INSTDIR\Clone.cmd w ;Opens a Empty File an fills it
   FileWrite $9 "cd $INSTDIR"
   FileSeek $9 0 END
   FileWrite $9 "$\r$\n" ; we write a new line
   
   FileWrite $9 "hg clone https://bitbucket.org/ptroen/star-skirmish-public"
   FileClose $9 ;Closes the filled file

         ExecWait '"$INSTDIR\Clone.cmd" "$INSTDIR" "$INSTDIR"'

FunctionEnd


Function nsDialogsPageMercudialCloneLeave
         	; 2) add mercudial clone command here
   ; hg clone http://selenic.com/hg
   Call Done
FunctionEnd

Section "Dummy Section" SecDummy

   ;  StrCpy $0 "Admin"
   ;  StrCpy "$Name" $0
    ; StrCpy "$Serial" "12345"

     # call userInfo plugin to get user info.  The plugin puts the result in the stack
    userInfo::getAccountType
   
    # pop the result from the stack into $0
    pop $0
 
    # compare the result with the string "Admin" to see if the user is admin.
    # If match, jump 3 lines down.
    strCmp $0 "Admin" +3

    # if there is not a match, print message and return
    messageBox MB_OK "This installer requires a User Account with 'Administrator' rights: $0"
    return


	; writing StarsAndDynasties Environmental variable
	Push "StarsAndDynasties"
	Push "$INSTDIR" 
	Call WriteEnvStr

	;Call nsDialogsPage

     CreateDirectory $INSTDIR
     
     ; execute the batch  file
     ; write the batch file




     WriteUninstaller "$INSTDIR\Uninst.exe"

     # create a shortcut named "new shortcut" in the start menu programs directory
     # point the new shortcut at the program uninstaller
     createShortCut "StarsAndDynasties\Uninstall.lnk" "$INSTDIR\uninst.exe"

SectionEnd



Section "Another Section"

     Var /GLOBAL "AnotherVar"

     StrCpy $AnotherVar "test"

SectionEnd

;--------------------------------
; Uninstaller

Section "Uninstall"

     StrCpy $Info "User variables test uninstalled successfully."
     Delete "$INSTDIR\Uninst.exe"
     RmDir $INSTDIR

SectionEnd

Function un.OnUninstSuccess

     HideWindow
     MessageBox MB_OK "$Info"
     
FunctionEnd
