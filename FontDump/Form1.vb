﻿Imports Microsoft.VisualBasic
Imports System
Imports System.IO
Imports System.Security.Permissions
Public Class Form1





    Dim intMaxFont As Integer
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' Font(0) = New Font("Arial", 32, FontStyle.Regular, GraphicsUnit.Pixel, 0)
        '  Font(1) = New Font("Arial Black", 32, FontStyle.Regular, GraphicsUnit.Pixel, 0)
        '  Font(2) = New Font("Courier New", 32, FontStyle.Regular, GraphicsUnit.Pixel, 0)
        'font(3) = New Font("Times New Roman", 32, FontStyle.Regular, GraphicsUnit.Pixel, 0)


        ' http://msdn.microsoft.com/en-us/library/szz5syt3(v=vs.80).aspx
        Dim sw As System.IO.BinaryWriter
        Dim TempBitmap As New Bitmap(pic.Image)
        sw = New System.IO.BinaryWriter(File.Open("W:\Temp\font.gui", FileMode.Create))

        For x As Integer = 0 To 8191
            For y As Integer = 0 To 8191
                Dim MyColor As Color
                MyColor = TempBitmap.GetPixel(x, y)
                sw.Write(MyColor.R)
                sw.Write(MyColor.G)
                sw.Write(MyColor.B)
                sw.Write(MyColor.A)

            Next
        Next

        sw.Close()
        intMaxFont = 6

    End Sub



    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        intMaxFont = 2



        For intFontIndex As Integer = 0 To intMaxFont Step 1
            Dim font As System.Drawing.Font

            Select Case intFontIndex
                Case 0
                    font = New Font("Arial", 16, FontStyle.Regular, GraphicsUnit.Pixel, 0)
                Case 1
                    font = New Font("Agency FB", 16, FontStyle.Regular, GraphicsUnit.Pixel, 0)
                Case 2
                    font = New Font("Algerian", 32, FontStyle.Regular, GraphicsUnit.Pixel, 0)
                Case 3
                    font = New Font("Arial Rounded MT", 32, FontStyle.Regular, GraphicsUnit.Pixel, 0)
                Case 4
                    font = New Font("Baskerville Old Face", 32, FontStyle.Regular, GraphicsUnit.Pixel, 0)
                Case 5
                    font = New Font("Bauhaus 93", 32, FontStyle.Regular, GraphicsUnit.Pixel, 0)
                Case 6
                    font = New Font("Bell MT", 8, FontStyle.Regular, GraphicsUnit.Pixel, 0)
                Case 7
                    font = New Font("Berlin Sans FB", 8, FontStyle.Regular, GraphicsUnit.Pixel, 0)
                Case 8
                    font = New Font("Bernard MT", 8, FontStyle.Regular, GraphicsUnit.Pixel, 0)
                Case 9
                    font = New Font("Blackadder ITC", 8, FontStyle.Regular, GraphicsUnit.Pixel, 0)
                Case 10
                    font = New Font("Bodoni MT", 8, FontStyle.Regular, GraphicsUnit.Pixel, 0)
                Case 11
                    font = New Font("Bodoni MT Poster", 8, FontStyle.Regular, GraphicsUnit.Pixel, 0)
                Case 12
                    font = New Font("Book Antiqua", 16, FontStyle.Regular, GraphicsUnit.Pixel, 0)
                Case 13
                    font = New Font("Bookman Old Style", 16, FontStyle.Regular, GraphicsUnit.Pixel, 0)
                Case 14
                    font = New Font("Bookshelf Symbol 7", 16, FontStyle.Regular, GraphicsUnit.Pixel, 0)
                Case 15
                    font = New Font("Bradley Hand ITC", 16, FontStyle.Regular, GraphicsUnit.Pixel, 0)
                Case 16
                    font = New Font("Britannic", 16, FontStyle.Regular, GraphicsUnit.Pixel, 0)
                Case 17
                    font = New Font("Broadway", 16, FontStyle.Regular, GraphicsUnit.Pixel, 0)
                Case 18
                    font = New Font("Brush Script MT", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 19
                    font = New Font("Calibri", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 20
                    font = New Font("Californian FB", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 21
                    font = New Font("Calisto MT", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 22
                    font = New Font("Cambria", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 23
                    font = New Font("Cambria Math", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 24
                    font = New Font("Candara", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 25
                    font = New Font("Castellar", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 26
                    font = New Font("Centaur", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 27
                    font = New Font("Century", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 28
                    font = New Font("Century Gothic", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 29
                    font = New Font("Century Schoolbook", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 30
                    font = New Font("Chiller", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 31
                    font = New Font("Colonna MT", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 32
                    font = New Font("Comic Sans MS", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 33
                    font = New Font("Consolas", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 34
                    font = New Font("Constantia", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 35
                    font = New Font("Cooper", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 36
                    font = New Font("Copperplate Gothic", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 37
                    font = New Font("Corbel", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 38
                    font = New Font("Courier New", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 39
                    font = New Font("Curlz MT", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 40
                    font = New Font("Edwardian Script ITC", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 41
                    font = New Font("Elephant", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 42
                    font = New Font("Engravers MT", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 43
                    font = New Font("Eras ITC", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 44
                    font = New Font("Felix Titling", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 45
                    font = New Font("Footlight MT", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 46
                    font = New Font("Forte", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 47
                    font = New Font("Franklin Gothic", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 48
                    font = New Font("Franklin Gothic Book", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 49
                    font = New Font("Freestyle Script", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 50
                    font = New Font("French Script MT", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 51
                    font = New Font("Gabriola", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 52
                    font = New Font("Garamond", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 53
                    font = New Font("Georgia", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 54
                    font = New Font("Gigi", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 55
                    font = New Font("Gill Sans", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 56
                    font = New Font("Gill Sans MT", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 57
                    font = New Font("Gloucester MT", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 58
                    font = New Font("Goudy Old Style", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 59
                    font = New Font("Goudy Stout", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 60
                    font = New Font("Haettenschweiler", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 61
                    font = New Font("Harlow Solid", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 62
                    font = New Font("Harrington", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 63
                    font = New Font("High Tower Text", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 64
                    font = New Font("Impact", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 65
                    font = New Font("Imprint MT Shadow", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 66
                    font = New Font("Informal Roman", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 67
                    font = New Font("Jokerman", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 68
                    font = New Font("Juice ITC", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 69
                    font = New Font("Kristen ITC", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 70
                    font = New Font("Kunstler Script", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 71
                    font = New Font("Latin", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 72
                    font = New Font("Lucida Bright", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 73
                    font = New Font("Lucida Calligraphy", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 74
                    font = New Font("Lucida Console", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 75
                    font = New Font("Lucida Fax", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 76
                    font = New Font("Lucida Handwriting", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 77
                    font = New Font("Lucida Sans", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 78
                    font = New Font("Lucida Sans Typewriter", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 79
                    font = New Font("Lucida Sans Unicode", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 80
                    font = New Font("Magneto", 16, FontStyle.Bold, GraphicsUnit.Pixel, 0)
                Case 81
                    font = New Font("Maiandra GD", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 82
                    font = New Font("Matura MT Script Capitals", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 83
                    font = New Font("Microsoft Sans Serif", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 84
                    font = New Font("Mistral", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 85
                    font = New Font("Modern No. 20", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 86
                    font = New Font("Monotype Corsiva", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 87
                    font = New Font("MS Outlook", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 88
                    font = New Font("MS Reference Sans Serif", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 89
                    font = New Font("MS Reference Specialty", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 90
                    font = New Font("MT Extra", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 91
                    font = New Font("Niagara Engraved", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 92
                    font = New Font("Niagara Solid", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 93
                    font = New Font("Nina", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 94
                    font = New Font("OCR A", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 95
                    font = New Font("Old English Text MT", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 96
                    font = New Font("Onyx", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 97
                    font = New Font("Palace Script MT", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 98
                    font = New Font("Palatino Linotype", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 99
                    font = New Font("Papyrus", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 100
                    font = New Font("Parchment", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 101
                    font = New Font("Perpetua", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 102
                    font = New Font("Perpetua Titling MT", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 103
                    font = New Font("Playbill", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 104
                    font = New Font("Poor Richard", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 105
                    font = New Font("Pristina", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 106
                    font = New Font("Rage", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 107
                    font = New Font("Ravie", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 108
                    font = New Font("Rockwell", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 109
                    font = New Font("Segoe", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 110
                    font = New Font("Segoe Print", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 111
                    font = New Font("Segoe Script", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 112
                    font = New Font("Segoe UI", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 113
                    font = New Font("Segoe UI Symbol", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 114
                    font = New Font("Showcard Gothic", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 115
                    font = New Font("Snap ITC", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 116
                    font = New Font("Stencil", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 117
                    font = New Font("SWGamekeys MT", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 118
                    font = New Font("SWMacro", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 119
                    font = New Font("Symbol", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 120
                    font = New Font("Tahoma", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 121
                    font = New Font("Tempus Sans ITC", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 122
                    font = New Font("Times New Roman", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 123
                    font = New Font("Trebuchet MS", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 124
                    font = New Font("Tw Cen MT", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 125
                    font = New Font("Verdana", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 126
                    font = New Font("Viner Hand ITC", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 127
                    font = New Font("Vivaldi", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 128
                    font = New Font("Vladimir Script", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 129
                    font = New Font("Webdings", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 130
                    font = New Font("Wingdings", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 131
                    font = New Font("Wingdings 2", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
                Case 116
                    font = New Font("Wingdings 3", 16, FontStyle.Italic, GraphicsUnit.Pixel, 0)
            End Select



            Dim bmp As Image
            bmp = New Bitmap(8192, (256 * 32))
            Dim g As Graphics = Graphics.FromImage(bmp)
            Dim col As System.Drawing.Color = Color.FromArgb(255, 0, 127, 255)
            Dim br As System.Drawing.Brush = New SolidBrush(col)

            For intX As Integer = 0 To 8191 Step 32

                For intY As Integer = 0 To 8191 Step 32

                    Dim intCounter As Integer = ((intX * intY) \ (32 * 32)) + (intX \ 32)
                    ' bmp_array(intCounter) = New Bitmap(32, 32)
                    'Dim g2 As Graphics = Graphics.FromImage(bmp_array(intCounter))
                    Dim intFontCounter As Integer = 0

                    Dim maxlength As Integer = 4096

                    Dim str As String = CStr(ChrW(intCounter))


                    ' g2.DrawString(str, font(intFontIndex), br, New RectangleF(0, 0, 31, 31))
                    ' If (DuplicateFont(intFontIndex) Or (intFontIndex = (intMaxFont - 1))) Then
                    ' http://msdn.microsoft.com/en-us/library/9wcdy991.aspx#Y0
                    Dim rectangle1 As New Rectangle(intX, intY, 32, 32)
                    g.DrawRectangle(Pens.Black, rectangle1)
                    g.DrawString(str, font, br, New RectangleF((intX + 8), (intY + 8), (31 + (intX) - 8), 31 + (intY - 8)))
                    'Else
                    '  g2.Clear(System.Drawing.Color.White)
                    ' End If

                    '   g.pi()

                    ' Dim Pixel As Color = g.
                Next
            Next
            Dim strFontName As String = font.Name
            Dim strPath As String = "W:\\images\\" + strFontName + ".png"
            bmp.Save(strPath)
        Next
        MessageBox.Show("Check W:\\images\\")
        Me.Close()
    End Sub

End Class
