﻿namespace EnvironmentEditor
{
    partial class Add_Image_Cross_Section
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnColorEmissionMap = new System.Windows.Forms.Button();
            this.btnDepthMap = new System.Windows.Forms.Button();
            this.btnAddAbsorptionMap = new System.Windows.Forms.Button();
            this.tbAddColors = new System.Windows.Forms.TabControl();
            this.tbColorEmission = new System.Windows.Forms.TabPage();
            this.picEmissionMap = new System.Windows.Forms.PictureBox();
            this.tbColorAbsorption = new System.Windows.Forms.TabPage();
            this.label27 = new System.Windows.Forms.Label();
            this.picColorAbsorption = new System.Windows.Forms.PictureBox();
            this.AddDepthZ = new System.Windows.Forms.TabPage();
            this.picEmmissionDepth = new System.Windows.Forms.PictureBox();
            this.tbAbsorptionDepth = new System.Windows.Forms.TabPage();
            this.picAbsorptionDepth = new System.Windows.Forms.PictureBox();
            this.btnAddAbsorptionDepth = new System.Windows.Forms.Button();
            this.tbAddAbsorption = new System.Windows.Forms.TabPage();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.picAbsNormalZ = new System.Windows.Forms.PictureBox();
            this.picAbsNormalY = new System.Windows.Forms.PictureBox();
            this.picAbsNormalX = new System.Windows.Forms.PictureBox();
            this.btnBumpMap = new System.Windows.Forms.Button();
            this.transparencyScatteringChannel = new System.Windows.Forms.TabPage();
            this.lnkCitation = new System.Windows.Forms.LinkLabel();
            this.picTransparency = new System.Windows.Forms.PictureBox();
            this.btnTransparencyMap = new System.Windows.Forms.Button();
            this.tbEmiTemp = new System.Windows.Forms.TabPage();
            this.picEmissionTemperature = new System.Windows.Forms.PictureBox();
            this.btnEmissionTemperature = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.picAbsTemp = new System.Windows.Forms.PictureBox();
            this.btnAddAbsoTemp = new System.Windows.Forms.Button();
            this.tabLightScatteringNormal = new System.Windows.Forms.TabPage();
            this.btnAddLightNormalZ = new System.Windows.Forms.Button();
            this.btnAddLightNormalY = new System.Windows.Forms.Button();
            this.picLightScatteringNormalZ = new System.Windows.Forms.PictureBox();
            this.picLightScatteringNormalY = new System.Windows.Forms.PictureBox();
            this.picLightScatteringNormalX = new System.Windows.Forms.PictureBox();
            this.btnAddLightNormalX = new System.Windows.Forms.Button();
            this.btnProcess = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_X = new System.Windows.Forms.TextBox();
            this.txt_Y = new System.Windows.Forms.TextBox();
            this.txt_Z = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbAddColors.SuspendLayout();
            this.tbColorEmission.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picEmissionMap)).BeginInit();
            this.tbColorAbsorption.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picColorAbsorption)).BeginInit();
            this.AddDepthZ.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picEmmissionDepth)).BeginInit();
            this.tbAbsorptionDepth.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picAbsorptionDepth)).BeginInit();
            this.tbAddAbsorption.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picAbsNormalZ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAbsNormalY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAbsNormalX)).BeginInit();
            this.transparencyScatteringChannel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picTransparency)).BeginInit();
            this.tbEmiTemp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picEmissionTemperature)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picAbsTemp)).BeginInit();
            this.tabLightScatteringNormal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLightScatteringNormalZ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLightScatteringNormalY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLightScatteringNormalX)).BeginInit();
            this.SuspendLayout();
            // 
            // btnColorEmissionMap
            // 
            this.btnColorEmissionMap.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnColorEmissionMap.Font = new System.Drawing.Font("Arial", 12F);
            this.btnColorEmissionMap.Location = new System.Drawing.Point(35, 26);
            this.btnColorEmissionMap.Margin = new System.Windows.Forms.Padding(5);
            this.btnColorEmissionMap.Name = "btnColorEmissionMap";
            this.btnColorEmissionMap.Size = new System.Drawing.Size(207, 58);
            this.btnColorEmissionMap.TabIndex = 66;
            this.btnColorEmissionMap.Text = "Add Emission Map";
            this.btnColorEmissionMap.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnColorEmissionMap.UseVisualStyleBackColor = false;
            this.btnColorEmissionMap.Click += new System.EventHandler(this.btnColorEmissionMap_Click);
            // 
            // btnDepthMap
            // 
            this.btnDepthMap.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnDepthMap.Font = new System.Drawing.Font("Arial", 12F);
            this.btnDepthMap.Location = new System.Drawing.Point(36, 20);
            this.btnDepthMap.Margin = new System.Windows.Forms.Padding(5);
            this.btnDepthMap.Name = "btnDepthMap";
            this.btnDepthMap.Size = new System.Drawing.Size(207, 58);
            this.btnDepthMap.TabIndex = 69;
            this.btnDepthMap.Text = "Add Depth(z) Map";
            this.btnDepthMap.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDepthMap.UseVisualStyleBackColor = false;
            this.btnDepthMap.Click += new System.EventHandler(this.btnDepthMap_Click);
            // 
            // btnAddAbsorptionMap
            // 
            this.btnAddAbsorptionMap.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnAddAbsorptionMap.Font = new System.Drawing.Font("Arial", 12F);
            this.btnAddAbsorptionMap.Location = new System.Drawing.Point(26, 34);
            this.btnAddAbsorptionMap.Margin = new System.Windows.Forms.Padding(5);
            this.btnAddAbsorptionMap.Name = "btnAddAbsorptionMap";
            this.btnAddAbsorptionMap.Size = new System.Drawing.Size(207, 58);
            this.btnAddAbsorptionMap.TabIndex = 70;
            this.btnAddAbsorptionMap.Text = "Add Absorption Map";
            this.btnAddAbsorptionMap.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddAbsorptionMap.UseVisualStyleBackColor = false;
            this.btnAddAbsorptionMap.Click += new System.EventHandler(this.btnAddAbsorptionMap_Click);
            // 
            // tbAddColors
            // 
            this.tbAddColors.Controls.Add(this.tbColorEmission);
            this.tbAddColors.Controls.Add(this.tbColorAbsorption);
            this.tbAddColors.Controls.Add(this.AddDepthZ);
            this.tbAddColors.Controls.Add(this.tbAbsorptionDepth);
            this.tbAddColors.Controls.Add(this.tbAddAbsorption);
            this.tbAddColors.Controls.Add(this.transparencyScatteringChannel);
            this.tbAddColors.Controls.Add(this.tbEmiTemp);
            this.tbAddColors.Controls.Add(this.tabPage2);
            this.tbAddColors.Controls.Add(this.tabLightScatteringNormal);
            this.tbAddColors.Location = new System.Drawing.Point(25, 12);
            this.tbAddColors.Name = "tbAddColors";
            this.tbAddColors.SelectedIndex = 0;
            this.tbAddColors.Size = new System.Drawing.Size(1147, 477);
            this.tbAddColors.TabIndex = 71;
            // 
            // tbColorEmission
            // 
            this.tbColorEmission.BackColor = System.Drawing.Color.Black;
            this.tbColorEmission.Controls.Add(this.picEmissionMap);
            this.tbColorEmission.Controls.Add(this.btnColorEmissionMap);
            this.tbColorEmission.Font = new System.Drawing.Font("Arial", 14F);
            this.tbColorEmission.Location = new System.Drawing.Point(4, 22);
            this.tbColorEmission.Name = "tbColorEmission";
            this.tbColorEmission.Padding = new System.Windows.Forms.Padding(3);
            this.tbColorEmission.Size = new System.Drawing.Size(1139, 451);
            this.tbColorEmission.TabIndex = 0;
            this.tbColorEmission.Text = "Emission";
            // 
            // picEmissionMap
            // 
            this.picEmissionMap.Location = new System.Drawing.Point(35, 105);
            this.picEmissionMap.Name = "picEmissionMap";
            this.picEmissionMap.Size = new System.Drawing.Size(671, 274);
            this.picEmissionMap.TabIndex = 71;
            this.picEmissionMap.TabStop = false;
            // 
            // tbColorAbsorption
            // 
            this.tbColorAbsorption.BackColor = System.Drawing.Color.Black;
            this.tbColorAbsorption.Controls.Add(this.label27);
            this.tbColorAbsorption.Controls.Add(this.picColorAbsorption);
            this.tbColorAbsorption.Controls.Add(this.btnAddAbsorptionMap);
            this.tbColorAbsorption.Location = new System.Drawing.Point(4, 22);
            this.tbColorAbsorption.Name = "tbColorAbsorption";
            this.tbColorAbsorption.Padding = new System.Windows.Forms.Padding(3);
            this.tbColorAbsorption.Size = new System.Drawing.Size(1139, 451);
            this.tbColorAbsorption.TabIndex = 1;
            this.tbColorAbsorption.Text = "Absorption";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Arial", 14F);
            this.label27.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label27.Location = new System.Drawing.Point(763, 17);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(107, 22);
            this.label27.TabIndex = 162;
            this.label27.Text = "Temp State";
            // 
            // picColorAbsorption
            // 
            this.picColorAbsorption.Location = new System.Drawing.Point(41, 100);
            this.picColorAbsorption.Name = "picColorAbsorption";
            this.picColorAbsorption.Size = new System.Drawing.Size(544, 292);
            this.picColorAbsorption.TabIndex = 71;
            this.picColorAbsorption.TabStop = false;
            // 
            // AddDepthZ
            // 
            this.AddDepthZ.BackColor = System.Drawing.Color.Black;
            this.AddDepthZ.Controls.Add(this.picEmmissionDepth);
            this.AddDepthZ.Controls.Add(this.btnDepthMap);
            this.AddDepthZ.Location = new System.Drawing.Point(4, 22);
            this.AddDepthZ.Name = "AddDepthZ";
            this.AddDepthZ.Padding = new System.Windows.Forms.Padding(3);
            this.AddDepthZ.Size = new System.Drawing.Size(1139, 451);
            this.AddDepthZ.TabIndex = 2;
            this.AddDepthZ.Text = "Add Emmision Depth";
            // 
            // picEmmissionDepth
            // 
            this.picEmmissionDepth.Location = new System.Drawing.Point(36, 102);
            this.picEmmissionDepth.Name = "picEmmissionDepth";
            this.picEmmissionDepth.Size = new System.Drawing.Size(544, 292);
            this.picEmmissionDepth.TabIndex = 71;
            this.picEmmissionDepth.TabStop = false;
            // 
            // tbAbsorptionDepth
            // 
            this.tbAbsorptionDepth.BackColor = System.Drawing.Color.Black;
            this.tbAbsorptionDepth.Controls.Add(this.picAbsorptionDepth);
            this.tbAbsorptionDepth.Controls.Add(this.btnAddAbsorptionDepth);
            this.tbAbsorptionDepth.Location = new System.Drawing.Point(4, 22);
            this.tbAbsorptionDepth.Name = "tbAbsorptionDepth";
            this.tbAbsorptionDepth.Padding = new System.Windows.Forms.Padding(3);
            this.tbAbsorptionDepth.Size = new System.Drawing.Size(1139, 451);
            this.tbAbsorptionDepth.TabIndex = 3;
            this.tbAbsorptionDepth.Text = "Add Absorption Depth Map";
            // 
            // picAbsorptionDepth
            // 
            this.picAbsorptionDepth.Location = new System.Drawing.Point(38, 100);
            this.picAbsorptionDepth.Name = "picAbsorptionDepth";
            this.picAbsorptionDepth.Size = new System.Drawing.Size(616, 345);
            this.picAbsorptionDepth.TabIndex = 71;
            this.picAbsorptionDepth.TabStop = false;
            // 
            // btnAddAbsorptionDepth
            // 
            this.btnAddAbsorptionDepth.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnAddAbsorptionDepth.Font = new System.Drawing.Font("Arial", 12F);
            this.btnAddAbsorptionDepth.Location = new System.Drawing.Point(27, 19);
            this.btnAddAbsorptionDepth.Margin = new System.Windows.Forms.Padding(5);
            this.btnAddAbsorptionDepth.Name = "btnAddAbsorptionDepth";
            this.btnAddAbsorptionDepth.Size = new System.Drawing.Size(207, 58);
            this.btnAddAbsorptionDepth.TabIndex = 70;
            this.btnAddAbsorptionDepth.Text = "Add Depth(z) Map";
            this.btnAddAbsorptionDepth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddAbsorptionDepth.UseVisualStyleBackColor = false;
            this.btnAddAbsorptionDepth.Click += new System.EventHandler(this.btnAddAbsorptionDepth_Click);
            // 
            // tbAddAbsorption
            // 
            this.tbAddAbsorption.BackColor = System.Drawing.Color.Black;
            this.tbAddAbsorption.Controls.Add(this.button2);
            this.tbAddAbsorption.Controls.Add(this.button1);
            this.tbAddAbsorption.Controls.Add(this.picAbsNormalZ);
            this.tbAddAbsorption.Controls.Add(this.picAbsNormalY);
            this.tbAddAbsorption.Controls.Add(this.picAbsNormalX);
            this.tbAddAbsorption.Controls.Add(this.btnBumpMap);
            this.tbAddAbsorption.Location = new System.Drawing.Point(4, 22);
            this.tbAddAbsorption.Name = "tbAddAbsorption";
            this.tbAddAbsorption.Padding = new System.Windows.Forms.Padding(3);
            this.tbAddAbsorption.Size = new System.Drawing.Size(1139, 451);
            this.tbAddAbsorption.TabIndex = 4;
            this.tbAddAbsorption.Text = "Add Absorption Normal";
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.CornflowerBlue;
            this.button2.Font = new System.Drawing.Font("Arial", 12F);
            this.button2.Location = new System.Drawing.Point(582, 331);
            this.button2.Margin = new System.Windows.Forms.Padding(5);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(235, 58);
            this.button2.TabIndex = 74;
            this.button2.Text = "Add Absorption Normal Map Z";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.CornflowerBlue;
            this.button1.Font = new System.Drawing.Font("Arial", 12F);
            this.button1.Location = new System.Drawing.Point(582, 224);
            this.button1.Margin = new System.Windows.Forms.Padding(5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(235, 58);
            this.button1.TabIndex = 73;
            this.button1.Text = "Add Absorption Normal Map Y";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // picAbsNormalZ
            // 
            this.picAbsNormalZ.Location = new System.Drawing.Point(30, 331);
            this.picAbsNormalZ.Name = "picAbsNormalZ";
            this.picAbsNormalZ.Size = new System.Drawing.Size(544, 92);
            this.picAbsNormalZ.TabIndex = 72;
            this.picAbsNormalZ.TabStop = false;
            // 
            // picAbsNormalY
            // 
            this.picAbsNormalY.Location = new System.Drawing.Point(30, 224);
            this.picAbsNormalY.Name = "picAbsNormalY";
            this.picAbsNormalY.Size = new System.Drawing.Size(544, 92);
            this.picAbsNormalY.TabIndex = 71;
            this.picAbsNormalY.TabStop = false;
            // 
            // picAbsNormalX
            // 
            this.picAbsNormalX.Location = new System.Drawing.Point(30, 106);
            this.picAbsNormalX.Name = "picAbsNormalX";
            this.picAbsNormalX.Size = new System.Drawing.Size(544, 92);
            this.picAbsNormalX.TabIndex = 70;
            this.picAbsNormalX.TabStop = false;
            // 
            // btnBumpMap
            // 
            this.btnBumpMap.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnBumpMap.Font = new System.Drawing.Font("Arial", 12F);
            this.btnBumpMap.Location = new System.Drawing.Point(582, 106);
            this.btnBumpMap.Margin = new System.Windows.Forms.Padding(5);
            this.btnBumpMap.Name = "btnBumpMap";
            this.btnBumpMap.Size = new System.Drawing.Size(235, 58);
            this.btnBumpMap.TabIndex = 69;
            this.btnBumpMap.Text = "Add Absorption Normal Map X";
            this.btnBumpMap.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBumpMap.UseVisualStyleBackColor = false;
            this.btnBumpMap.Click += new System.EventHandler(this.btnBumpMap_Click);
            // 
            // transparencyScatteringChannel
            // 
            this.transparencyScatteringChannel.BackColor = System.Drawing.Color.Black;
            this.transparencyScatteringChannel.Controls.Add(this.lnkCitation);
            this.transparencyScatteringChannel.Controls.Add(this.picTransparency);
            this.transparencyScatteringChannel.Controls.Add(this.btnTransparencyMap);
            this.transparencyScatteringChannel.Location = new System.Drawing.Point(4, 22);
            this.transparencyScatteringChannel.Name = "transparencyScatteringChannel";
            this.transparencyScatteringChannel.Padding = new System.Windows.Forms.Padding(3);
            this.transparencyScatteringChannel.Size = new System.Drawing.Size(1139, 451);
            this.transparencyScatteringChannel.TabIndex = 5;
            this.transparencyScatteringChannel.Text = "Transparency Light Scattering Channel";
            // 
            // lnkCitation
            // 
            this.lnkCitation.AutoSize = true;
            this.lnkCitation.CausesValidation = false;
            this.lnkCitation.Location = new System.Drawing.Point(664, 38);
            this.lnkCitation.Name = "lnkCitation";
            this.lnkCitation.Size = new System.Drawing.Size(236, 13);
            this.lnkCitation.TabIndex = 73;
            this.lnkCitation.TabStop = true;
            this.lnkCitation.Text = "http://en.wikipedia.org/wiki/Rayleigh_scattering";
            // 
            // picTransparency
            // 
            this.picTransparency.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picTransparency.Location = new System.Drawing.Point(26, 93);
            this.picTransparency.Name = "picTransparency";
            this.picTransparency.Size = new System.Drawing.Size(913, 307);
            this.picTransparency.TabIndex = 72;
            this.picTransparency.TabStop = false;
            // 
            // btnTransparencyMap
            // 
            this.btnTransparencyMap.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnTransparencyMap.Font = new System.Drawing.Font("Arial", 12F);
            this.btnTransparencyMap.Location = new System.Drawing.Point(26, 17);
            this.btnTransparencyMap.Margin = new System.Windows.Forms.Padding(5);
            this.btnTransparencyMap.Name = "btnTransparencyMap";
            this.btnTransparencyMap.Size = new System.Drawing.Size(207, 58);
            this.btnTransparencyMap.TabIndex = 71;
            this.btnTransparencyMap.Text = "Add Transparency Light Scattering Channel";
            this.btnTransparencyMap.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTransparencyMap.UseVisualStyleBackColor = false;
            this.btnTransparencyMap.Click += new System.EventHandler(this.btnTransparencyMap_Click);
            // 
            // tbEmiTemp
            // 
            this.tbEmiTemp.BackColor = System.Drawing.Color.Black;
            this.tbEmiTemp.Controls.Add(this.picEmissionTemperature);
            this.tbEmiTemp.Controls.Add(this.btnEmissionTemperature);
            this.tbEmiTemp.Location = new System.Drawing.Point(4, 22);
            this.tbEmiTemp.Name = "tbEmiTemp";
            this.tbEmiTemp.Padding = new System.Windows.Forms.Padding(3);
            this.tbEmiTemp.Size = new System.Drawing.Size(1139, 451);
            this.tbEmiTemp.TabIndex = 6;
            this.tbEmiTemp.Text = "Emission Temperature";
            // 
            // picEmissionTemperature
            // 
            this.picEmissionTemperature.Location = new System.Drawing.Point(26, 84);
            this.picEmissionTemperature.Name = "picEmissionTemperature";
            this.picEmissionTemperature.Size = new System.Drawing.Size(544, 292);
            this.picEmissionTemperature.TabIndex = 73;
            this.picEmissionTemperature.TabStop = false;
            // 
            // btnEmissionTemperature
            // 
            this.btnEmissionTemperature.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnEmissionTemperature.Font = new System.Drawing.Font("Arial", 12F);
            this.btnEmissionTemperature.Location = new System.Drawing.Point(11, 18);
            this.btnEmissionTemperature.Margin = new System.Windows.Forms.Padding(5);
            this.btnEmissionTemperature.Name = "btnEmissionTemperature";
            this.btnEmissionTemperature.Size = new System.Drawing.Size(207, 58);
            this.btnEmissionTemperature.TabIndex = 72;
            this.btnEmissionTemperature.Text = "Add Emission Temperature Map";
            this.btnEmissionTemperature.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEmissionTemperature.UseVisualStyleBackColor = false;
            this.btnEmissionTemperature.Click += new System.EventHandler(this.btnEmissionTemperature_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.Black;
            this.tabPage2.Controls.Add(this.picAbsTemp);
            this.tabPage2.Controls.Add(this.btnAddAbsoTemp);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1139, 451);
            this.tabPage2.TabIndex = 7;
            this.tabPage2.Text = "Absorption Temperature";
            // 
            // picAbsTemp
            // 
            this.picAbsTemp.Location = new System.Drawing.Point(67, 117);
            this.picAbsTemp.Name = "picAbsTemp";
            this.picAbsTemp.Size = new System.Drawing.Size(888, 295);
            this.picAbsTemp.TabIndex = 73;
            this.picAbsTemp.TabStop = false;
            // 
            // btnAddAbsoTemp
            // 
            this.btnAddAbsoTemp.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnAddAbsoTemp.Font = new System.Drawing.Font("Arial", 12F);
            this.btnAddAbsoTemp.Location = new System.Drawing.Point(52, 51);
            this.btnAddAbsoTemp.Margin = new System.Windows.Forms.Padding(5);
            this.btnAddAbsoTemp.Name = "btnAddAbsoTemp";
            this.btnAddAbsoTemp.Size = new System.Drawing.Size(207, 58);
            this.btnAddAbsoTemp.TabIndex = 72;
            this.btnAddAbsoTemp.Text = "Add Absorption Temperature Map";
            this.btnAddAbsoTemp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddAbsoTemp.UseVisualStyleBackColor = false;
            this.btnAddAbsoTemp.Click += new System.EventHandler(this.btnAddAbsoTemp_Click);
            // 
            // tabLightScatteringNormal
            // 
            this.tabLightScatteringNormal.BackColor = System.Drawing.Color.Black;
            this.tabLightScatteringNormal.Controls.Add(this.btnAddLightNormalZ);
            this.tabLightScatteringNormal.Controls.Add(this.btnAddLightNormalY);
            this.tabLightScatteringNormal.Controls.Add(this.picLightScatteringNormalZ);
            this.tabLightScatteringNormal.Controls.Add(this.picLightScatteringNormalY);
            this.tabLightScatteringNormal.Controls.Add(this.picLightScatteringNormalX);
            this.tabLightScatteringNormal.Controls.Add(this.btnAddLightNormalX);
            this.tabLightScatteringNormal.Location = new System.Drawing.Point(4, 22);
            this.tabLightScatteringNormal.Name = "tabLightScatteringNormal";
            this.tabLightScatteringNormal.Padding = new System.Windows.Forms.Padding(3);
            this.tabLightScatteringNormal.Size = new System.Drawing.Size(1139, 451);
            this.tabLightScatteringNormal.TabIndex = 8;
            this.tabLightScatteringNormal.Text = "Light Scattering Normal";
            // 
            // btnAddLightNormalZ
            // 
            this.btnAddLightNormalZ.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnAddLightNormalZ.Font = new System.Drawing.Font("Arial", 12F);
            this.btnAddLightNormalZ.Location = new System.Drawing.Point(677, 292);
            this.btnAddLightNormalZ.Margin = new System.Windows.Forms.Padding(5);
            this.btnAddLightNormalZ.Name = "btnAddLightNormalZ";
            this.btnAddLightNormalZ.Size = new System.Drawing.Size(235, 58);
            this.btnAddLightNormalZ.TabIndex = 80;
            this.btnAddLightNormalZ.Text = "Add Light Scattering Normal Map Z";
            this.btnAddLightNormalZ.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddLightNormalZ.UseVisualStyleBackColor = false;
            this.btnAddLightNormalZ.Click += new System.EventHandler(this.btnAddLightNormalZ_Click);
            // 
            // btnAddLightNormalY
            // 
            this.btnAddLightNormalY.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnAddLightNormalY.Font = new System.Drawing.Font("Arial", 12F);
            this.btnAddLightNormalY.Location = new System.Drawing.Point(677, 185);
            this.btnAddLightNormalY.Margin = new System.Windows.Forms.Padding(5);
            this.btnAddLightNormalY.Name = "btnAddLightNormalY";
            this.btnAddLightNormalY.Size = new System.Drawing.Size(235, 58);
            this.btnAddLightNormalY.TabIndex = 79;
            this.btnAddLightNormalY.Text = "Add Light Scattering Normal Map Y";
            this.btnAddLightNormalY.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddLightNormalY.UseVisualStyleBackColor = false;
            this.btnAddLightNormalY.Click += new System.EventHandler(this.btnAddLightNormalY_Click);
            // 
            // picLightScatteringNormalZ
            // 
            this.picLightScatteringNormalZ.Location = new System.Drawing.Point(125, 292);
            this.picLightScatteringNormalZ.Name = "picLightScatteringNormalZ";
            this.picLightScatteringNormalZ.Size = new System.Drawing.Size(544, 92);
            this.picLightScatteringNormalZ.TabIndex = 78;
            this.picLightScatteringNormalZ.TabStop = false;
            // 
            // picLightScatteringNormalY
            // 
            this.picLightScatteringNormalY.Location = new System.Drawing.Point(125, 185);
            this.picLightScatteringNormalY.Name = "picLightScatteringNormalY";
            this.picLightScatteringNormalY.Size = new System.Drawing.Size(544, 92);
            this.picLightScatteringNormalY.TabIndex = 77;
            this.picLightScatteringNormalY.TabStop = false;
            // 
            // picLightScatteringNormalX
            // 
            this.picLightScatteringNormalX.Location = new System.Drawing.Point(125, 67);
            this.picLightScatteringNormalX.Name = "picLightScatteringNormalX";
            this.picLightScatteringNormalX.Size = new System.Drawing.Size(544, 92);
            this.picLightScatteringNormalX.TabIndex = 76;
            this.picLightScatteringNormalX.TabStop = false;
            // 
            // btnAddLightNormalX
            // 
            this.btnAddLightNormalX.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnAddLightNormalX.Font = new System.Drawing.Font("Arial", 12F);
            this.btnAddLightNormalX.Location = new System.Drawing.Point(677, 67);
            this.btnAddLightNormalX.Margin = new System.Windows.Forms.Padding(5);
            this.btnAddLightNormalX.Name = "btnAddLightNormalX";
            this.btnAddLightNormalX.Size = new System.Drawing.Size(235, 58);
            this.btnAddLightNormalX.TabIndex = 75;
            this.btnAddLightNormalX.Text = "Add Light Scattering Normal Map X";
            this.btnAddLightNormalX.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddLightNormalX.UseVisualStyleBackColor = false;
            this.btnAddLightNormalX.Click += new System.EventHandler(this.btnAddLightNormalX_Click);
            // 
            // btnProcess
            // 
            this.btnProcess.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnProcess.Font = new System.Drawing.Font("Arial", 12F);
            this.btnProcess.Location = new System.Drawing.Point(604, 493);
            this.btnProcess.Margin = new System.Windows.Forms.Padding(5);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(294, 54);
            this.btnProcess.TabIndex = 72;
            this.btnProcess.Text = "Process Volumetric Cross Section";
            this.btnProcess.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnProcess.UseVisualStyleBackColor = false;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 14F);
            this.label1.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label1.Location = new System.Drawing.Point(236, 515);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(23, 22);
            this.label1.TabIndex = 163;
            this.label1.Text = "X";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 14F);
            this.label2.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label2.Location = new System.Drawing.Point(25, 515);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(171, 22);
            this.label2.TabIndex = 164;
            this.label2.Text = "Translate of Centre";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 14F);
            this.label3.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label3.Location = new System.Drawing.Point(349, 515);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(22, 22);
            this.label3.TabIndex = 165;
            this.label3.Text = "Y";
            // 
            // txt_X
            // 
            this.txt_X.BackColor = System.Drawing.Color.CornflowerBlue;
            this.txt_X.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_X.Font = new System.Drawing.Font("Arial", 14F);
            this.txt_X.ForeColor = System.Drawing.Color.Black;
            this.txt_X.Location = new System.Drawing.Point(269, 515);
            this.txt_X.Margin = new System.Windows.Forms.Padding(5);
            this.txt_X.Name = "txt_X";
            this.txt_X.Size = new System.Drawing.Size(70, 22);
            this.txt_X.TabIndex = 166;
            this.txt_X.Text = "0";
            // 
            // txt_Y
            // 
            this.txt_Y.BackColor = System.Drawing.Color.CornflowerBlue;
            this.txt_Y.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_Y.Font = new System.Drawing.Font("Arial", 14F);
            this.txt_Y.ForeColor = System.Drawing.Color.Black;
            this.txt_Y.Location = new System.Drawing.Point(381, 515);
            this.txt_Y.Margin = new System.Windows.Forms.Padding(5);
            this.txt_Y.Name = "txt_Y";
            this.txt_Y.Size = new System.Drawing.Size(70, 22);
            this.txt_Y.TabIndex = 167;
            this.txt_Y.Text = "0";
            // 
            // txt_Z
            // 
            this.txt_Z.BackColor = System.Drawing.Color.CornflowerBlue;
            this.txt_Z.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_Z.Font = new System.Drawing.Font("Arial", 14F);
            this.txt_Z.ForeColor = System.Drawing.Color.Black;
            this.txt_Z.Location = new System.Drawing.Point(498, 515);
            this.txt_Z.Margin = new System.Windows.Forms.Padding(5);
            this.txt_Z.Name = "txt_Z";
            this.txt_Z.Size = new System.Drawing.Size(70, 22);
            this.txt_Z.TabIndex = 169;
            this.txt_Z.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 14F);
            this.label4.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label4.Location = new System.Drawing.Point(461, 515);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(22, 22);
            this.label4.TabIndex = 168;
            this.label4.Text = "Z";
            // 
            // Add_Image_Cross_Section
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(1184, 553);
            this.Controls.Add(this.txt_Z);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txt_Y);
            this.Controls.Add(this.txt_X);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnProcess);
            this.Controls.Add(this.tbAddColors);
            this.Name = "Add_Image_Cross_Section";
            this.Text = "Add_Image_Cross_Section";
            this.Load += new System.EventHandler(this.Add_Image_Cross_Section_Load);
            this.tbAddColors.ResumeLayout(false);
            this.tbColorEmission.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picEmissionMap)).EndInit();
            this.tbColorAbsorption.ResumeLayout(false);
            this.tbColorAbsorption.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picColorAbsorption)).EndInit();
            this.AddDepthZ.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picEmmissionDepth)).EndInit();
            this.tbAbsorptionDepth.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picAbsorptionDepth)).EndInit();
            this.tbAddAbsorption.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picAbsNormalZ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAbsNormalY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAbsNormalX)).EndInit();
            this.transparencyScatteringChannel.ResumeLayout(false);
            this.transparencyScatteringChannel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picTransparency)).EndInit();
            this.tbEmiTemp.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picEmissionTemperature)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picAbsTemp)).EndInit();
            this.tabLightScatteringNormal.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picLightScatteringNormalZ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLightScatteringNormalY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLightScatteringNormalX)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnColorEmissionMap;
        private System.Windows.Forms.Button btnDepthMap;
        private System.Windows.Forms.Button btnAddAbsorptionMap;
        private System.Windows.Forms.TabControl tbAddColors;
        private System.Windows.Forms.TabPage tbColorEmission;
        private System.Windows.Forms.TabPage tbColorAbsorption;
        private System.Windows.Forms.TabPage AddDepthZ;
        private System.Windows.Forms.TabPage tbAbsorptionDepth;
        private System.Windows.Forms.TabPage tbAddAbsorption;
        private System.Windows.Forms.Button btnBumpMap;
        private System.Windows.Forms.Button btnAddAbsorptionDepth;
        private System.Windows.Forms.PictureBox picEmmissionDepth;
        private System.Windows.Forms.PictureBox picAbsorptionDepth;
        private System.Windows.Forms.PictureBox picAbsNormalX;
        private System.Windows.Forms.PictureBox picEmissionMap;
        private System.Windows.Forms.PictureBox picColorAbsorption;
        private System.Windows.Forms.Button btnProcess;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.TabPage transparencyScatteringChannel;
        private System.Windows.Forms.LinkLabel lnkCitation;
        private System.Windows.Forms.PictureBox picTransparency;
        private System.Windows.Forms.Button btnTransparencyMap;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TabPage tbEmiTemp;
        private System.Windows.Forms.PictureBox picEmissionTemperature;
        private System.Windows.Forms.Button btnEmissionTemperature;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.PictureBox picAbsTemp;
        private System.Windows.Forms.Button btnAddAbsoTemp;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_X;
        private System.Windows.Forms.TextBox txt_Y;
        private System.Windows.Forms.TextBox txt_Z;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox picAbsNormalZ;
        private System.Windows.Forms.PictureBox picAbsNormalY;
        private System.Windows.Forms.TabPage tabLightScatteringNormal;
        private System.Windows.Forms.Button btnAddLightNormalZ;
        private System.Windows.Forms.Button btnAddLightNormalY;
        private System.Windows.Forms.PictureBox picLightScatteringNormalZ;
        private System.Windows.Forms.PictureBox picLightScatteringNormalY;
        private System.Windows.Forms.PictureBox picLightScatteringNormalX;
        private System.Windows.Forms.Button btnAddLightNormalX;
    }
}