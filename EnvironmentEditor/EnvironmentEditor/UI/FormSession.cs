﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
// http://stackoverflow.com/questions/11591002/how-can-i-use-sqlite-in-a-c-sharp-project
namespace EnvironmentEditor
{
    public partial class FormSession : Form
    {
        
     public   Spectrum_Editor spec;
       Controller control;
      public  UnitTest unittest;
      public VolumeTextureControl voltexcontrol;

        public FormSession()
        {
            Globals.formsession = this;
            control = new Controller();
            InitializeComponent();
            spec = new Spectrum_Editor();
            RenderCmbSession();
        }

        void RenderCmbSession()
        {
    
            DataTable dt=new DataTable();
            control.GetSession( ref dt);
            //cmbSession.Items.Clear();
            /*
            foreach (DataRow row in dt.Rows) // Loop over the rows.
            {
                string s2="";
                 foreach (var item in row.ItemArray) // Loop over the items.
	            {
                       s2+=item.ToString()+" ";
                   
	            }
              
                cmbSession.Items.Add(s2);
            }*/
        
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        //    btnEditEntity.Hide();
      //      btnEditPixel.Hide();
       //     btnEditStats.Hide();
      //      btnEditVolumeTexture.Hide();
        //    btnUnitTest.Hide();

        }

        private void cmbSession_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
               // string str=this.cmbSession.SelectedIndex.ToString();
               // Globals.iSessionID = Convert.ToInt32(str);
            }
            catch (Exception ex)
            {
                
              
            }
           
            btnEditEntity.Show();
            btnEditPixel.Show();
            btnEditStats.Show();
            btnEditVolumeTexture.Show();
          //  btnUnitTest.Show();
       
        }

        private void button2_Click(object sender, EventArgs e)       {       }

        private void btnEditPixel_Click(object sender, EventArgs e)
        {

        spec.Show();
       this.Hide();
        }

        private void btnEditEntity_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            unittest = new UnitTest();
            unittest.Show();
        }

        private void btnEditVolumeTexture_Click(object sender, EventArgs e)
        {
            voltexcontrol = new VolumeTextureControl();
            voltexcontrol.Show();
            this.Hide();
        }

 
    }
}
