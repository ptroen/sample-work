﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EnvironmentEditor
{
    public partial class Add_Image_Cross_Section : Form
    {
        ImageCrossSection crosssection;
        Controller control;
        public Add_Image_Cross_Section()
        {
            crosssection = new ImageCrossSection();
            control = new Controller();
            InitializeComponent();
        }

        private void Add_Image_Cross_Section_Load(object sender, EventArgs e)   {      }

        void ProcessDialog(ref PictureBox pic)
        {
            openFileDialog = new OpenFileDialog();
            openFileDialog.ShowDialog();
            pic.Load(openFileDialog.FileName);
        }
        Texture Process4Channels(ref PictureBox pic)
        {
           ProcessDialog(ref  pic);
           return Texture.process_texture(ref pic, 4);
        }
       Texture TextureProcess3Channels(ref PictureBox pic)
        {
            ProcessDialog(ref  pic);
            return Texture.process_texture(ref pic, 3);
        }

        Texture Process2Channels(ref PictureBox pic)
        {
            ProcessDialog(ref  pic);
            return Texture.process_texture(ref pic, 2);
        }

        Texture Process1Channel(ref PictureBox pic)
        {
            ProcessDialog(ref  pic);
            return Texture.process_texture(ref pic, 1);
        }

        private void btnColorEmissionMap_Click(object sender, EventArgs e)   
        {
            Texture t = Process2Channels(ref picEmissionMap);
            crosssection.SetEmission(ref t);
        }

        private void btnAddAbsorptionMap_Click(object sender, EventArgs e)
        {
            Texture t = Process2Channels(ref picColorAbsorption);
            crosssection.SetAbsorption(ref t);
        }

        private void btnDepthMap_Click(object sender, EventArgs e)
        {
            Texture t=Process1Channel(ref picEmmissionDepth);
            crosssection.SetEmissionDepth(ref t);
        }

        private void btnAddAbsorptionDepth_Click(object sender, EventArgs e)
        {
            Texture t = Process1Channel(ref picAbsorptionDepth);
            crosssection.SetAbsorptionDepth(ref t);
        }

        private void btnBumpMap_Click(object sender, EventArgs e)
        {  // this in Normal X_Click actually....
            Texture t = Process4Channels(ref  picAbsNormalX);
            crosssection.SetAbsorptionNormalX(ref t);
        }

        private void btnTransparencyMap_Click(object sender, EventArgs e)
        {
            Texture t = Process4Channels(ref  picTransparency);
            crosssection.SetTransparencyLightScatteringChannel(ref t);
        }

        private void btnEmissionTemperature_Click(object sender, EventArgs e)
        {
            Texture t = Process4Channels(ref picEmissionTemperature);
            crosssection.SetEmissionTemperature(ref t);
        }

        private void btnAddAbsoTemp_Click(object sender, EventArgs e)
        {
            Texture t = Process4Channels(ref picAbsTemp);
            crosssection.SetAbsorptionTemperature(ref t);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Texture t = Process4Channels(ref picAbsNormalY);
            crosssection.SetAbsorptionNormalY(ref t);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Texture t = Process4Channels(ref picAbsNormalZ);
            crosssection.SetAbsorptionNormalZ(ref t);
        }

        private void btnAddLightNormalX_Click(object sender, EventArgs e)
        {
            Texture t = Process4Channels(ref picLightScatteringNormalX);
            crosssection.SetLightScatteringNormalX(ref t);
        }

        private void btnAddLightNormalY_Click(object sender, EventArgs e)
        {
            Texture t = Process4Channels(ref picLightScatteringNormalY);
            crosssection.SetLightScatteringNormalY(ref t);
        }

        private void btnAddLightNormalZ_Click(object sender, EventArgs e)
        {
            Texture t = Process4Channels(ref picLightScatteringNormalZ);
            crosssection.SetLightScatteringNormalZ(ref t);
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            int X, Y, Z;
            X = Convert.ToInt32(txt_X.Text);
            Y = Convert.ToInt32(txt_Y.Text);
            Z = Convert.ToInt32(txt_Z.Text);
            int i = Globals.volume_id;
            control.ReplaceIntoCrossSection(ref crosssection, i);
        }
    }
}
