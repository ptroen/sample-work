﻿namespace EnvironmentEditor
{
    partial class FormSession
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.sessionNameDataSet = new EnvironmentEditor.SessionNameDataSet();
            this.sessionNameBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.session_NameTableAdapter = new EnvironmentEditor.SessionNameDataSetTableAdapters.Session_NameTableAdapter();
            this.sessionNameBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.gameDataSet1 = new EnvironmentEditor.gameDataSet1();
            this.session_NameTableAdapter1 = new EnvironmentEditor.gameDataSet1TableAdapters.Session_NameTableAdapter();
            this.btnEditPixel = new System.Windows.Forms.Button();
            this.btnEditEntity = new System.Windows.Forms.Button();
            this.btnEditVolumeTexture = new System.Windows.Forms.Button();
            this.btnEditStats = new System.Windows.Forms.Button();
            this.btnUnitTest = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.sessionNameDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionNameBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionNameBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gameDataSet1)).BeginInit();
            this.SuspendLayout();
            // 
            // sessionNameDataSet
            // 
            this.sessionNameDataSet.DataSetName = "SessionNameDataSet";
            this.sessionNameDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sessionNameBindingSource
            // 
            this.sessionNameBindingSource.DataMember = "Session_Name";
            this.sessionNameBindingSource.DataSource = this.sessionNameDataSet;
            // 
            // session_NameTableAdapter
            // 
            this.session_NameTableAdapter.ClearBeforeFill = true;
            // 
            // sessionNameBindingSource1
            // 
            this.sessionNameBindingSource1.DataMember = "Session_Name";
            this.sessionNameBindingSource1.DataSource = this.gameDataSet1;
            // 
            // gameDataSet1
            // 
            this.gameDataSet1.DataSetName = "gameDataSet1";
            this.gameDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // session_NameTableAdapter1
            // 
            this.session_NameTableAdapter1.ClearBeforeFill = true;
            // 
            // btnEditPixel
            // 
            this.btnEditPixel.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnEditPixel.Font = new System.Drawing.Font("Arial Black", 14F);
            this.btnEditPixel.ForeColor = System.Drawing.Color.Black;
            this.btnEditPixel.Location = new System.Drawing.Point(167, 139);
            this.btnEditPixel.Name = "btnEditPixel";
            this.btnEditPixel.Size = new System.Drawing.Size(408, 33);
            this.btnEditPixel.TabIndex = 1;
            this.btnEditPixel.Text = "Edit Pixel";
            this.btnEditPixel.UseVisualStyleBackColor = false;
            this.btnEditPixel.Click += new System.EventHandler(this.btnEditPixel_Click);
            // 
            // btnEditEntity
            // 
            this.btnEditEntity.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnEditEntity.Font = new System.Drawing.Font("Arial Black", 14F);
            this.btnEditEntity.ForeColor = System.Drawing.Color.Black;
            this.btnEditEntity.Location = new System.Drawing.Point(167, 216);
            this.btnEditEntity.Name = "btnEditEntity";
            this.btnEditEntity.Size = new System.Drawing.Size(408, 32);
            this.btnEditEntity.TabIndex = 5;
            this.btnEditEntity.Text = "Edit Entity";
            this.btnEditEntity.UseVisualStyleBackColor = false;
            this.btnEditEntity.Click += new System.EventHandler(this.btnEditEntity_Click);
            // 
            // btnEditVolumeTexture
            // 
            this.btnEditVolumeTexture.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnEditVolumeTexture.Font = new System.Drawing.Font("Arial Black", 14F);
            this.btnEditVolumeTexture.ForeColor = System.Drawing.Color.Black;
            this.btnEditVolumeTexture.Location = new System.Drawing.Point(167, 178);
            this.btnEditVolumeTexture.Name = "btnEditVolumeTexture";
            this.btnEditVolumeTexture.Size = new System.Drawing.Size(408, 32);
            this.btnEditVolumeTexture.TabIndex = 6;
            this.btnEditVolumeTexture.Text = "Edit Volume Texture";
            this.btnEditVolumeTexture.UseVisualStyleBackColor = false;
            this.btnEditVolumeTexture.Click += new System.EventHandler(this.btnEditVolumeTexture_Click);
            // 
            // btnEditStats
            // 
            this.btnEditStats.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnEditStats.Font = new System.Drawing.Font("Arial Black", 14F);
            this.btnEditStats.Location = new System.Drawing.Point(167, 254);
            this.btnEditStats.Name = "btnEditStats";
            this.btnEditStats.Size = new System.Drawing.Size(408, 32);
            this.btnEditStats.TabIndex = 7;
            this.btnEditStats.Text = "Edit Stats";
            this.btnEditStats.UseVisualStyleBackColor = false;
            // 
            // btnUnitTest
            // 
            this.btnUnitTest.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnUnitTest.Font = new System.Drawing.Font("Arial Black", 14F);
            this.btnUnitTest.Location = new System.Drawing.Point(167, 292);
            this.btnUnitTest.Name = "btnUnitTest";
            this.btnUnitTest.Size = new System.Drawing.Size(408, 32);
            this.btnUnitTest.TabIndex = 8;
            this.btnUnitTest.Text = "Unit Test";
            this.btnUnitTest.UseVisualStyleBackColor = false;
            this.btnUnitTest.Click += new System.EventHandler(this.button1_Click);
            // 
            // FormSession
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(754, 464);
            this.Controls.Add(this.btnUnitTest);
            this.Controls.Add(this.btnEditStats);
            this.Controls.Add(this.btnEditVolumeTexture);
            this.Controls.Add(this.btnEditEntity);
            this.Controls.Add(this.btnEditPixel);
            this.ForeColor = System.Drawing.Color.Black;
            this.Name = "FormSession";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.sessionNameDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionNameBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sessionNameBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gameDataSet1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private SessionNameDataSet sessionNameDataSet;
        private System.Windows.Forms.BindingSource sessionNameBindingSource;
        private SessionNameDataSetTableAdapters.Session_NameTableAdapter session_NameTableAdapter;
        private gameDataSet1 gameDataSet1;
        private System.Windows.Forms.BindingSource sessionNameBindingSource1;
        private gameDataSet1TableAdapters.Session_NameTableAdapter session_NameTableAdapter1;
        private System.Windows.Forms.Button btnEditPixel;
        private System.Windows.Forms.Button btnEditEntity;
        private System.Windows.Forms.Button btnEditVolumeTexture;
        private System.Windows.Forms.Button btnEditStats;
        private System.Windows.Forms.Button btnUnitTest;

    }
}

