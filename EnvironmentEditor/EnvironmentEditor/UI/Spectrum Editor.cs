﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
namespace EnvironmentEditor
{
    public partial class Spectrum_Editor : Form
    {
        Spectrum spec;
        Controller control;
        string spectrumname;
     //   int iTemperature_in_K;
        int temperature_state;
        int iWavelength_in_nm;
        int id;
       short Iintensity;
        public Spectrum_Editor()
        {
            id =-1;
            spectrumname = System.String.Empty;
            control = new Controller();
            temperature_state = 0;
            spec = new Spectrum();
            InitializeComponent();
      //      iTemperature_in_K = 0;
            Iintensity = 0;
            Spectrum_update();
        }
        private void Spectrum_Editor_Load(object sender, EventArgs e){ Spectrum_update();     }
        void Spectrum_update()
        {
            RenderCmbSpectrum();
            RefreshColorSample();
            DrawSpectra();    
        }
        void RenderCmbSpectrum()
        {
            DataTable dt = new DataTable();
            try
            {
                control.GetSpectrum(ref dt);
                cmbSpectrum.Items.Clear();
                foreach (DataRow row in dt.Rows) // Loop over the rows.
                {
                    string s2 = "";
                    foreach (var item in row.ItemArray) // Loop over the items.
                    {
                        s2 += item.ToString() + " ";
                    }
                    cmbSpectrum.Items.Add(s2);
                }
            }
            catch (Exception e)  { }
        }
        private void cmbSpectrum_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string str = this.cmbSpectrum.SelectedIndex.ToString();
                this.id = Convert.ToInt32(str);
            }
            catch (Exception ex)   {   }
        }

        private System.Drawing.Graphics GrabSpectrumControl(int i)
        {
            switch (i)
            {
                case 1: return this.pic_spectrum1.CreateGraphics();
                case 2: return this.pic_spectrum2.CreateGraphics();
                case 3: return this.pic_spectrum3.CreateGraphics();
                case 4: return this.pic_spectrum4.CreateGraphics();
                case 5: return this.pic_spectrum5.CreateGraphics();
                case 6: return this.pic_spectrum6.CreateGraphics();
                case 7: return this.pic_spectrum7.CreateGraphics();
                case 8: return this.pic_spectrum8.CreateGraphics();
                case 9: return this.pic_spectrum9.CreateGraphics();
                case 10: return this.pic_spectrum10.CreateGraphics();
                case 11: return this.pic_spectrum11.CreateGraphics();
                case 12: return this.pic_spectrum12.CreateGraphics();
                case 13: return this.pic_spectrum13.CreateGraphics();
                case 14: return this.pic_spectrum14.CreateGraphics();
                case 15: return this.pic_spectrum15.CreateGraphics();
             
            }
            return this.pic_spectrum.CreateGraphics();
        }

        private  System.Drawing.Graphics GrabSpectrumResultant(int i)
        {
            switch (i)
            {
                case 1: return this.picSpectrumResultantColor1.CreateGraphics();
                case 2: return this.picSpectrumResultantColor2.CreateGraphics();
                case 3: return this.picSpectrumResultantColor3.CreateGraphics();
                case 4: return this.picSpectrumResultantColor4.CreateGraphics();
                case 5: return this.picSpectrumResultantColor5.CreateGraphics();
                case 6: return this.picSpectrumResultantColor6.CreateGraphics();
                case 7: return this.picSpectrumResultantColor7.CreateGraphics();
                case 8: return this.picSpectrumResultantColor8.CreateGraphics();
                case 9: return this.picSpectrumResultantColor9.CreateGraphics();
                case 10: return this.picSpectrumResultantColor10.CreateGraphics();
                case 11: return this.picSpectrumResultantColor11.CreateGraphics();
                case 12: return this.picSpectrumResultantColor12.CreateGraphics();
                case 13: return this.picSpectrumResultantColor13.CreateGraphics();
                case 14: return this.picSpectrumResultantColor14.CreateGraphics();
                case 15: return this.picSpectrumResultantColor15.CreateGraphics();
            }
            return this.picSpectrumResultantColor.CreateGraphics();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string s = cmbSpectrum.SelectedText;
         //   Controller.ReplaceIntoSpectrum(ref Globals.db,ref spec, ref sessionname,Globals.iSessionID);
            RenderCmbSpectrum();
        }
        private void txtSpectrumName_TextChanged(object sender, EventArgs e) {  }
        private void pic_emmission_Click(object sender, EventArgs e)   {Spectrum_update();  }
        void DrawSpectra()
        {
            for (int i = 0; i < 16; i++)
            {
            System.Drawing.Graphics g =GrabSpectrumControl(i); 
            g.Clear(Color.Black);
            System.Drawing.Color rgb = spec.DrawSpectrum(ref g, i);
                // Drawing rgb color
                //tRed.Text = rgb.R.ToString(); txtGreen.Text = rgb.G.ToString(); txtBlue.Text = rgb.B.ToString();
                g = GrabSpectrumResultant(i);
                g.Clear(Color.Black);
                System.Drawing.SolidBrush br = new SolidBrush(rgb);
                g.FillRectangle(br, 0, 0, this.picSpectrumResultantColor.ClientSize.Width - 1, picSpectrumResultantColor.ClientSize.Height - 1);
            }
           
      
        }
        private void txt_Temp_TextChanged(object sender, EventArgs e){ }
        void RefreshColorSample()
        {
            this.iWavelength_in_nm = Convert.ToInt32(txt_wavelength_in_nm.Text);
            try
            {
                if (this.iWavelength_in_nm > 300 && this.iWavelength_in_nm <= 650)
                    this.iWavelength_in_nm -= this.iWavelength_in_nm % 10;
                else if (this.iWavelength_in_nm < 300) this.iWavelength_in_nm = 300;
                else this.iWavelength_in_nm = 650;
            }
            catch (System.Exception ex)    {   }
            txt_wavelength_in_nm.Text = Convert.ToString(this.iWavelength_in_nm);
                ViewSpectrum view = new ViewSpectrum();
                view.getColorFromWaveLength((iWavelength_in_nm/10) *10, this.Iintensity);
            System.Drawing.Color col=view.GetColor();
           	System.Drawing.SolidBrush br=new SolidBrush(col);
            System.Drawing.Graphics g = this.picColorSample.CreateGraphics();
            g.FillRectangle(br, 0, 0, this.picColorSample.ClientSize.Width - 1, this.picColorSample.ClientSize.Height - 1);      
        }
        private void txt_wavelength_in_nm_TextChanged(object sender, EventArgs e)   {  RefreshColorSample();   }
        private void txtIntensity_TextChanged(object sender, EventArgs e) {
            try  {
                this.Iintensity = Convert.ToInt16(txtIntensity.Text);
                Spectrum_update();  } catch(Exception ex){} }

        private void btn_λ_Click(object sender, EventArgs e)
        {
            spec.SetSpectrum( this.Iintensity,this.iWavelength_in_nm,this.temperature_state);
            Spectrum_update();
        }
        void ClearButton()
        {
            spec.Clear(this.temperature_state,this.Iintensity);
            Spectrum_update();
        }
        private void button1_Click(object sender, EventArgs e)  {  ClearButton();   }
        private void button3_Click(object sender, EventArgs e)  { spec.ExportCSV();      }
        private void button4_Click(object sender, EventArgs e)
        {
            spec.ImportCSV();
            Spectrum_update();
        }
        private void btnCycleColorTemp_Click(object sender, EventArgs e){ }
        private void txtDestTemp_TextChanged(object sender, EventArgs e){  }
        private void txtDeltaTemp_TextChanged(object sender, EventArgs e) {  }
        private void picSpecColor_Click(object sender, EventArgs e)  { }

        private void pic_spectrum13_Click(object sender, EventArgs e)  {      }

        private void cmbTemperatureState_SelectedIndexChanged(object sender, EventArgs e)
        {
            try    {   this.temperature_state = Convert.ToInt32(cmbTemperatureState.Text);   }
            catch (System.Exception ex) { }   
        }

        private void txtInput_TextChanged(object sender, EventArgs e)
        {

        }

        void ReplaceIntoSpectrum()
        {
            spectrumname = this.txtInput.Text;
            id = this.control.ReplaceIntoSpectrum(ref this.spec, spectrumname,id);
           RenderCmbSpectrum();
        }
        private void btnInsert_Click(object sender, EventArgs e)
        {
            ReplaceIntoSpectrum();
            RenderCmbSpectrum();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            ReplaceIntoSpectrum();
            RenderCmbSpectrum();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            string s = "";
            s=this.cmbSpectrum.Text.ToString();
            string[] words = s.Split(' ');
            try
            {
                id = Convert.ToInt32(words[0]);
                control.LoadSpectrum(ref spec, words[0]);
                RenderCmbSpectrum();
                Spectrum_update();
            }
            catch (System.Exception ex)
            {
            	
            }
         
        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                control.DeleteSpectrum(id);
                this.Close();
                Globals.formsession.spec = new Spectrum_Editor();
                Globals.formsession.spec.Show();
            }
            catch (System.Exception ex)
            {
            	    
            }
         
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Hide();
            Globals.formsession.Show();
        }
    }
}
