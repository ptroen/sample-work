﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EnvironmentEditor
{
    public partial class UnitTest : Form
    {
        public UnitTest()
        {
            InitializeComponent();
        }

        private void UnitTest_Load(object sender, EventArgs e)
        {
        }

        private void btnUnitTest_Click(object sender, EventArgs e)
        {
            List<string> s = Test_Spectrum.UnitTest();
            txtUnit.Clear();
            string ss="";
            foreach (var item in s)
            {
                ss += item + System.Environment.NewLine;
            }
            txtUnit.Text = ss;
        }
    }
}
