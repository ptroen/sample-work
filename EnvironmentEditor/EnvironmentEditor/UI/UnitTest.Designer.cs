﻿namespace EnvironmentEditor
{
    partial class UnitTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtUnit = new System.Windows.Forms.TextBox();
            this.btnUnitTest = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtUnit
            // 
            this.txtUnit.Location = new System.Drawing.Point(25, 91);
            this.txtUnit.Multiline = true;
            this.txtUnit.Name = "txtUnit";
            this.txtUnit.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtUnit.Size = new System.Drawing.Size(1161, 409);
            this.txtUnit.TabIndex = 0;
            // 
            // btnUnitTest
            // 
            this.btnUnitTest.Location = new System.Drawing.Point(25, 29);
            this.btnUnitTest.Name = "btnUnitTest";
            this.btnUnitTest.Size = new System.Drawing.Size(1161, 42);
            this.btnUnitTest.TabIndex = 1;
            this.btnUnitTest.Text = "Unit Test";
            this.btnUnitTest.UseVisualStyleBackColor = true;
            this.btnUnitTest.Click += new System.EventHandler(this.btnUnitTest_Click);
            // 
            // UnitTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1243, 526);
            this.Controls.Add(this.btnUnitTest);
            this.Controls.Add(this.txtUnit);
            this.Name = "UnitTest";
            this.Text = "UnitTest";
            this.Load += new System.EventHandler(this.UnitTest_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtUnit;
        private System.Windows.Forms.Button btnUnitTest;
    }
}