﻿namespace EnvironmentEditor
{
    partial class Spectrum_Editor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbSpectrum = new System.Windows.Forms.ComboBox();
            this.btn_λ = new System.Windows.Forms.Button();
            this.pic_spectrum = new System.Windows.Forms.PictureBox();
            this.picColorSample = new System.Windows.Forms.PictureBox();
            this.txt_wavelength_in_nm = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtIntensity = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.picSpectrumResultantColor = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.cmbTemperatureState = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.picSpectrumResultantColor1 = new System.Windows.Forms.PictureBox();
            this.pic_spectrum1 = new System.Windows.Forms.PictureBox();
            this.picSpectrumResultantColor2 = new System.Windows.Forms.PictureBox();
            this.pic_spectrum2 = new System.Windows.Forms.PictureBox();
            this.picSpectrumResultantColor3 = new System.Windows.Forms.PictureBox();
            this.pic_spectrum3 = new System.Windows.Forms.PictureBox();
            this.picSpectrumResultantColor7 = new System.Windows.Forms.PictureBox();
            this.pic_spectrum7 = new System.Windows.Forms.PictureBox();
            this.picSpectrumResultantColor6 = new System.Windows.Forms.PictureBox();
            this.pic_spectrum6 = new System.Windows.Forms.PictureBox();
            this.picSpectrumResultantColor5 = new System.Windows.Forms.PictureBox();
            this.pic_spectrum5 = new System.Windows.Forms.PictureBox();
            this.picSpectrumResultantColor4 = new System.Windows.Forms.PictureBox();
            this.pic_spectrum4 = new System.Windows.Forms.PictureBox();
            this.picSpectrumResultantColor12 = new System.Windows.Forms.PictureBox();
            this.pic_spectrum12 = new System.Windows.Forms.PictureBox();
            this.picSpectrumResultantColor11 = new System.Windows.Forms.PictureBox();
            this.pic_spectrum11 = new System.Windows.Forms.PictureBox();
            this.picSpectrumResultantColor10 = new System.Windows.Forms.PictureBox();
            this.pic_spectrum10 = new System.Windows.Forms.PictureBox();
            this.picSpectrumResultantColor9 = new System.Windows.Forms.PictureBox();
            this.pic_spectrum9 = new System.Windows.Forms.PictureBox();
            this.picSpectrumResultantColor8 = new System.Windows.Forms.PictureBox();
            this.pic_spectrum8 = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.picSpectrumResultantColor15 = new System.Windows.Forms.PictureBox();
            this.pic_spectrum15 = new System.Windows.Forms.PictureBox();
            this.picSpectrumResultantColor14 = new System.Windows.Forms.PictureBox();
            this.pic_spectrum14 = new System.Windows.Forms.PictureBox();
            this.picSpectrumResultantColor13 = new System.Windows.Forms.PictureBox();
            this.pic_spectrum13 = new System.Windows.Forms.PictureBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnInsert = new System.Windows.Forms.Button();
            this.txtInput = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pic_spectrum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picColorSample)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSpectrumResultantColor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSpectrumResultantColor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_spectrum1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSpectrumResultantColor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_spectrum2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSpectrumResultantColor3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_spectrum3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSpectrumResultantColor7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_spectrum7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSpectrumResultantColor6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_spectrum6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSpectrumResultantColor5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_spectrum5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSpectrumResultantColor4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_spectrum4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSpectrumResultantColor12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_spectrum12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSpectrumResultantColor11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_spectrum11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSpectrumResultantColor10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_spectrum10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSpectrumResultantColor9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_spectrum9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSpectrumResultantColor8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_spectrum8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSpectrumResultantColor15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_spectrum15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSpectrumResultantColor14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_spectrum14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSpectrumResultantColor13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_spectrum13)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbSpectrum
            // 
            this.cmbSpectrum.FormattingEnabled = true;
            this.cmbSpectrum.Location = new System.Drawing.Point(1016, 13);
            this.cmbSpectrum.Name = "cmbSpectrum";
            this.cmbSpectrum.Size = new System.Drawing.Size(259, 21);
            this.cmbSpectrum.TabIndex = 0;
            this.cmbSpectrum.SelectedIndexChanged += new System.EventHandler(this.cmbSpectrum_SelectedIndexChanged);
            // 
            // btn_λ
            // 
            this.btn_λ.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btn_λ.Font = new System.Drawing.Font("Arial", 12F);
            this.btn_λ.Location = new System.Drawing.Point(280, 52);
            this.btn_λ.Margin = new System.Windows.Forms.Padding(5);
            this.btn_λ.Name = "btn_λ";
            this.btn_λ.Size = new System.Drawing.Size(186, 61);
            this.btn_λ.TabIndex = 65;
            this.btn_λ.Text = "Update λ and intensity";
            this.btn_λ.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_λ.UseVisualStyleBackColor = false;
            this.btn_λ.Click += new System.EventHandler(this.btn_λ_Click);
            // 
            // pic_spectrum
            // 
            this.pic_spectrum.Location = new System.Drawing.Point(210, 156);
            this.pic_spectrum.Name = "pic_spectrum";
            this.pic_spectrum.Size = new System.Drawing.Size(954, 45);
            this.pic_spectrum.TabIndex = 63;
            this.pic_spectrum.TabStop = false;
            this.pic_spectrum.Click += new System.EventHandler(this.pic_emmission_Click);
            // 
            // picColorSample
            // 
            this.picColorSample.Location = new System.Drawing.Point(195, 53);
            this.picColorSample.Name = "picColorSample";
            this.picColorSample.Size = new System.Drawing.Size(77, 61);
            this.picColorSample.TabIndex = 60;
            this.picColorSample.TabStop = false;
            // 
            // txt_wavelength_in_nm
            // 
            this.txt_wavelength_in_nm.BackColor = System.Drawing.Color.CornflowerBlue;
            this.txt_wavelength_in_nm.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_wavelength_in_nm.Font = new System.Drawing.Font("Arial", 14F);
            this.txt_wavelength_in_nm.ForeColor = System.Drawing.Color.Black;
            this.txt_wavelength_in_nm.Location = new System.Drawing.Point(13, 91);
            this.txt_wavelength_in_nm.Margin = new System.Windows.Forms.Padding(5);
            this.txt_wavelength_in_nm.Name = "txt_wavelength_in_nm";
            this.txt_wavelength_in_nm.Size = new System.Drawing.Size(70, 22);
            this.txt_wavelength_in_nm.TabIndex = 59;
            this.txt_wavelength_in_nm.Text = "599";
            this.txt_wavelength_in_nm.TextChanged += new System.EventHandler(this.txt_wavelength_in_nm_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 14F);
            this.label5.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label5.Location = new System.Drawing.Point(9, 52);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 22);
            this.label5.TabIndex = 58;
            this.label5.Text = "λ in nm";
            // 
            // txtIntensity
            // 
            this.txtIntensity.BackColor = System.Drawing.Color.CornflowerBlue;
            this.txtIntensity.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtIntensity.Font = new System.Drawing.Font("Arial", 14F);
            this.txtIntensity.ForeColor = System.Drawing.Color.Black;
            this.txtIntensity.Location = new System.Drawing.Point(107, 92);
            this.txtIntensity.Margin = new System.Windows.Forms.Padding(5);
            this.txtIntensity.Name = "txtIntensity";
            this.txtIntensity.Size = new System.Drawing.Size(70, 22);
            this.txtIntensity.TabIndex = 72;
            this.txtIntensity.Text = "0";
            this.txtIntensity.TextChanged += new System.EventHandler(this.txtIntensity_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 14F);
            this.label2.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label2.Location = new System.Drawing.Point(103, 53);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 22);
            this.label2.TabIndex = 71;
            this.label2.Text = "Intensity";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.CornflowerBlue;
            this.button1.Font = new System.Drawing.Font("Arial", 12F);
            this.button1.Location = new System.Drawing.Point(476, 52);
            this.button1.Margin = new System.Windows.Forms.Padding(5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(148, 61);
            this.button1.TabIndex = 73;
            this.button1.Text = "Uniform Intensity";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // picSpectrumResultantColor
            // 
            this.picSpectrumResultantColor.Location = new System.Drawing.Point(1194, 155);
            this.picSpectrumResultantColor.Name = "picSpectrumResultantColor";
            this.picSpectrumResultantColor.Size = new System.Drawing.Size(67, 45);
            this.picSpectrumResultantColor.TabIndex = 74;
            this.picSpectrumResultantColor.TabStop = false;
            this.picSpectrumResultantColor.Click += new System.EventHandler(this.picSpecColor_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 14F);
            this.label4.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label4.Location = new System.Drawing.Point(1132, 120);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(143, 22);
            this.label4.TabIndex = 75;
            this.label4.Text = "Spectrum Color";
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.CornflowerBlue;
            this.button3.Font = new System.Drawing.Font("Arial", 12F);
            this.button3.Location = new System.Drawing.Point(634, 52);
            this.button3.Margin = new System.Windows.Forms.Padding(5);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(121, 61);
            this.button3.TabIndex = 76;
            this.button3.Text = "Export to CSV";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.CornflowerBlue;
            this.button4.Font = new System.Drawing.Font("Arial", 12F);
            this.button4.Location = new System.Drawing.Point(765, 52);
            this.button4.Margin = new System.Windows.Forms.Padding(5);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(120, 61);
            this.button4.TabIndex = 77;
            this.button4.Text = "Import from CSV";
            this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // cmbTemperatureState
            // 
            this.cmbTemperatureState.FormattingEnabled = true;
            this.cmbTemperatureState.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15"});
            this.cmbTemperatureState.Location = new System.Drawing.Point(183, 10);
            this.cmbTemperatureState.Name = "cmbTemperatureState";
            this.cmbTemperatureState.Size = new System.Drawing.Size(259, 21);
            this.cmbTemperatureState.TabIndex = 89;
            this.cmbTemperatureState.SelectedIndexChanged += new System.EventHandler(this.cmbTemperatureState_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 14F);
            this.label1.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label1.Location = new System.Drawing.Point(14, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(161, 22);
            this.label1.TabIndex = 90;
            this.label1.Text = "TemperatureState";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 14F);
            this.label6.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label6.Location = new System.Drawing.Point(569, 120);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(141, 22);
            this.label6.TabIndex = 92;
            this.label6.Text = "State Spectrum";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 14F);
            this.label7.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label7.Location = new System.Drawing.Point(129, 120);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(117, 22);
            this.label7.TabIndex = 93;
            this.label7.Text = "Temperature";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 10F);
            this.label11.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label11.Location = new System.Drawing.Point(122, 218);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(80, 16);
            this.label11.TabIndex = 96;
            this.label11.Text = "16K/-257 C";
            // 
            // picSpectrumResultantColor1
            // 
            this.picSpectrumResultantColor1.Location = new System.Drawing.Point(1194, 218);
            this.picSpectrumResultantColor1.Name = "picSpectrumResultantColor1";
            this.picSpectrumResultantColor1.Size = new System.Drawing.Size(67, 45);
            this.picSpectrumResultantColor1.TabIndex = 95;
            this.picSpectrumResultantColor1.TabStop = false;
            // 
            // pic_spectrum1
            // 
            this.pic_spectrum1.Location = new System.Drawing.Point(210, 218);
            this.pic_spectrum1.Name = "pic_spectrum1";
            this.pic_spectrum1.Size = new System.Drawing.Size(954, 45);
            this.pic_spectrum1.TabIndex = 94;
            this.pic_spectrum1.TabStop = false;
            // 
            // picSpectrumResultantColor2
            // 
            this.picSpectrumResultantColor2.Location = new System.Drawing.Point(1194, 275);
            this.picSpectrumResultantColor2.Name = "picSpectrumResultantColor2";
            this.picSpectrumResultantColor2.Size = new System.Drawing.Size(67, 45);
            this.picSpectrumResultantColor2.TabIndex = 98;
            this.picSpectrumResultantColor2.TabStop = false;
            // 
            // pic_spectrum2
            // 
            this.pic_spectrum2.Location = new System.Drawing.Point(210, 275);
            this.pic_spectrum2.Name = "pic_spectrum2";
            this.pic_spectrum2.Size = new System.Drawing.Size(954, 45);
            this.pic_spectrum2.TabIndex = 97;
            this.pic_spectrum2.TabStop = false;
            // 
            // picSpectrumResultantColor3
            // 
            this.picSpectrumResultantColor3.Location = new System.Drawing.Point(1194, 327);
            this.picSpectrumResultantColor3.Name = "picSpectrumResultantColor3";
            this.picSpectrumResultantColor3.Size = new System.Drawing.Size(67, 45);
            this.picSpectrumResultantColor3.TabIndex = 101;
            this.picSpectrumResultantColor3.TabStop = false;
            // 
            // pic_spectrum3
            // 
            this.pic_spectrum3.Location = new System.Drawing.Point(210, 327);
            this.pic_spectrum3.Name = "pic_spectrum3";
            this.pic_spectrum3.Size = new System.Drawing.Size(954, 45);
            this.pic_spectrum3.TabIndex = 100;
            this.pic_spectrum3.TabStop = false;
            // 
            // picSpectrumResultantColor7
            // 
            this.picSpectrumResultantColor7.Location = new System.Drawing.Point(1194, 556);
            this.picSpectrumResultantColor7.Name = "picSpectrumResultantColor7";
            this.picSpectrumResultantColor7.Size = new System.Drawing.Size(67, 45);
            this.picSpectrumResultantColor7.TabIndex = 113;
            this.picSpectrumResultantColor7.TabStop = false;
            // 
            // pic_spectrum7
            // 
            this.pic_spectrum7.Location = new System.Drawing.Point(210, 556);
            this.pic_spectrum7.Name = "pic_spectrum7";
            this.pic_spectrum7.Size = new System.Drawing.Size(954, 45);
            this.pic_spectrum7.TabIndex = 112;
            this.pic_spectrum7.TabStop = false;
            // 
            // picSpectrumResultantColor6
            // 
            this.picSpectrumResultantColor6.Location = new System.Drawing.Point(1194, 504);
            this.picSpectrumResultantColor6.Name = "picSpectrumResultantColor6";
            this.picSpectrumResultantColor6.Size = new System.Drawing.Size(67, 45);
            this.picSpectrumResultantColor6.TabIndex = 110;
            this.picSpectrumResultantColor6.TabStop = false;
            // 
            // pic_spectrum6
            // 
            this.pic_spectrum6.Location = new System.Drawing.Point(210, 504);
            this.pic_spectrum6.Name = "pic_spectrum6";
            this.pic_spectrum6.Size = new System.Drawing.Size(954, 45);
            this.pic_spectrum6.TabIndex = 109;
            this.pic_spectrum6.TabStop = false;
            // 
            // picSpectrumResultantColor5
            // 
            this.picSpectrumResultantColor5.Location = new System.Drawing.Point(1194, 447);
            this.picSpectrumResultantColor5.Name = "picSpectrumResultantColor5";
            this.picSpectrumResultantColor5.Size = new System.Drawing.Size(67, 45);
            this.picSpectrumResultantColor5.TabIndex = 107;
            this.picSpectrumResultantColor5.TabStop = false;
            // 
            // pic_spectrum5
            // 
            this.pic_spectrum5.Location = new System.Drawing.Point(210, 447);
            this.pic_spectrum5.Name = "pic_spectrum5";
            this.pic_spectrum5.Size = new System.Drawing.Size(954, 45);
            this.pic_spectrum5.TabIndex = 106;
            this.pic_spectrum5.TabStop = false;
            // 
            // picSpectrumResultantColor4
            // 
            this.picSpectrumResultantColor4.Location = new System.Drawing.Point(1194, 384);
            this.picSpectrumResultantColor4.Name = "picSpectrumResultantColor4";
            this.picSpectrumResultantColor4.Size = new System.Drawing.Size(67, 45);
            this.picSpectrumResultantColor4.TabIndex = 104;
            this.picSpectrumResultantColor4.TabStop = false;
            // 
            // pic_spectrum4
            // 
            this.pic_spectrum4.Location = new System.Drawing.Point(210, 384);
            this.pic_spectrum4.Name = "pic_spectrum4";
            this.pic_spectrum4.Size = new System.Drawing.Size(954, 45);
            this.pic_spectrum4.TabIndex = 103;
            this.pic_spectrum4.TabStop = false;
            // 
            // picSpectrumResultantColor12
            // 
            this.picSpectrumResultantColor12.Location = new System.Drawing.Point(1194, 840);
            this.picSpectrumResultantColor12.Name = "picSpectrumResultantColor12";
            this.picSpectrumResultantColor12.Size = new System.Drawing.Size(67, 45);
            this.picSpectrumResultantColor12.TabIndex = 128;
            this.picSpectrumResultantColor12.TabStop = false;
            // 
            // pic_spectrum12
            // 
            this.pic_spectrum12.Location = new System.Drawing.Point(210, 840);
            this.pic_spectrum12.Name = "pic_spectrum12";
            this.pic_spectrum12.Size = new System.Drawing.Size(954, 45);
            this.pic_spectrum12.TabIndex = 127;
            this.pic_spectrum12.TabStop = false;
            this.pic_spectrum12.Click += new System.EventHandler(this.pic_spectrum13_Click);
            // 
            // picSpectrumResultantColor11
            // 
            this.picSpectrumResultantColor11.Location = new System.Drawing.Point(1194, 783);
            this.picSpectrumResultantColor11.Name = "picSpectrumResultantColor11";
            this.picSpectrumResultantColor11.Size = new System.Drawing.Size(67, 45);
            this.picSpectrumResultantColor11.TabIndex = 125;
            this.picSpectrumResultantColor11.TabStop = false;
            // 
            // pic_spectrum11
            // 
            this.pic_spectrum11.Location = new System.Drawing.Point(210, 783);
            this.pic_spectrum11.Name = "pic_spectrum11";
            this.pic_spectrum11.Size = new System.Drawing.Size(954, 45);
            this.pic_spectrum11.TabIndex = 124;
            this.pic_spectrum11.TabStop = false;
            // 
            // picSpectrumResultantColor10
            // 
            this.picSpectrumResultantColor10.Location = new System.Drawing.Point(1194, 731);
            this.picSpectrumResultantColor10.Name = "picSpectrumResultantColor10";
            this.picSpectrumResultantColor10.Size = new System.Drawing.Size(67, 45);
            this.picSpectrumResultantColor10.TabIndex = 122;
            this.picSpectrumResultantColor10.TabStop = false;
            // 
            // pic_spectrum10
            // 
            this.pic_spectrum10.Location = new System.Drawing.Point(210, 731);
            this.pic_spectrum10.Name = "pic_spectrum10";
            this.pic_spectrum10.Size = new System.Drawing.Size(954, 45);
            this.pic_spectrum10.TabIndex = 121;
            this.pic_spectrum10.TabStop = false;
            // 
            // picSpectrumResultantColor9
            // 
            this.picSpectrumResultantColor9.Location = new System.Drawing.Point(1194, 674);
            this.picSpectrumResultantColor9.Name = "picSpectrumResultantColor9";
            this.picSpectrumResultantColor9.Size = new System.Drawing.Size(67, 45);
            this.picSpectrumResultantColor9.TabIndex = 119;
            this.picSpectrumResultantColor9.TabStop = false;
            // 
            // pic_spectrum9
            // 
            this.pic_spectrum9.Location = new System.Drawing.Point(210, 674);
            this.pic_spectrum9.Name = "pic_spectrum9";
            this.pic_spectrum9.Size = new System.Drawing.Size(954, 45);
            this.pic_spectrum9.TabIndex = 118;
            this.pic_spectrum9.TabStop = false;
            // 
            // picSpectrumResultantColor8
            // 
            this.picSpectrumResultantColor8.Location = new System.Drawing.Point(1194, 611);
            this.picSpectrumResultantColor8.Name = "picSpectrumResultantColor8";
            this.picSpectrumResultantColor8.Size = new System.Drawing.Size(67, 45);
            this.picSpectrumResultantColor8.TabIndex = 116;
            this.picSpectrumResultantColor8.TabStop = false;
            // 
            // pic_spectrum8
            // 
            this.pic_spectrum8.Location = new System.Drawing.Point(210, 611);
            this.pic_spectrum8.Name = "pic_spectrum8";
            this.pic_spectrum8.Size = new System.Drawing.Size(954, 45);
            this.pic_spectrum8.TabIndex = 115;
            this.pic_spectrum8.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 10F);
            this.label3.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label3.Location = new System.Drawing.Point(130, 156);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 16);
            this.label3.TabIndex = 91;
            this.label3.Text = "0K/-273 C";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 10F);
            this.label12.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label12.Location = new System.Drawing.Point(122, 275);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(80, 16);
            this.label12.TabIndex = 141;
            this.label12.Text = "32K/-241 C";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 10F);
            this.label13.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label13.Location = new System.Drawing.Point(122, 327);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(80, 16);
            this.label13.TabIndex = 142;
            this.label13.Text = "64K/-209 C";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 10F);
            this.label14.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label14.Location = new System.Drawing.Point(114, 384);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(88, 16);
            this.label14.TabIndex = 143;
            this.label14.Text = "128K/-145 C";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 10F);
            this.label15.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label15.Location = new System.Drawing.Point(122, 447);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(80, 16);
            this.label15.TabIndex = 144;
            this.label15.Text = "256K/-17 C";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 10F);
            this.label16.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label16.Location = new System.Drawing.Point(119, 504);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(83, 16);
            this.label16.TabIndex = 145;
            this.label16.Text = "512K/238 C";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial", 10F);
            this.label17.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label17.Location = new System.Drawing.Point(111, 556);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(91, 16);
            this.label17.TabIndex = 146;
            this.label17.Text = "1024K/750 C";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Arial", 10F);
            this.label21.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label21.Location = new System.Drawing.Point(153, 611);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(49, 16);
            this.label21.TabIndex = 147;
            this.label21.Text = "2048K";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Arial", 10F);
            this.label22.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label22.Location = new System.Drawing.Point(153, 674);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(49, 16);
            this.label22.TabIndex = 148;
            this.label22.Text = "4096K";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Arial", 10F);
            this.label23.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label23.Location = new System.Drawing.Point(153, 731);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(49, 16);
            this.label23.TabIndex = 149;
            this.label23.Text = "8192K";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Arial", 10F);
            this.label24.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label24.Location = new System.Drawing.Point(145, 783);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(57, 16);
            this.label24.TabIndex = 150;
            this.label24.Text = "16384K";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Arial", 10F);
            this.label25.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label25.Location = new System.Drawing.Point(145, 840);
            this.label25.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(57, 16);
            this.label25.TabIndex = 151;
            this.label25.Text = "32768K";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial", 10F);
            this.label18.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label18.Location = new System.Drawing.Point(137, 1000);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(65, 16);
            this.label18.TabIndex = 160;
            this.label18.Text = "262144K";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Arial", 10F);
            this.label19.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label19.Location = new System.Drawing.Point(137, 943);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(65, 16);
            this.label19.TabIndex = 159;
            this.label19.Text = "131072K";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Arial", 10F);
            this.label20.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label20.Location = new System.Drawing.Point(145, 891);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(57, 16);
            this.label20.TabIndex = 158;
            this.label20.Text = "65536K";
            // 
            // picSpectrumResultantColor15
            // 
            this.picSpectrumResultantColor15.Location = new System.Drawing.Point(1194, 1000);
            this.picSpectrumResultantColor15.Name = "picSpectrumResultantColor15";
            this.picSpectrumResultantColor15.Size = new System.Drawing.Size(67, 45);
            this.picSpectrumResultantColor15.TabIndex = 157;
            this.picSpectrumResultantColor15.TabStop = false;
            // 
            // pic_spectrum15
            // 
            this.pic_spectrum15.Location = new System.Drawing.Point(210, 1000);
            this.pic_spectrum15.Name = "pic_spectrum15";
            this.pic_spectrum15.Size = new System.Drawing.Size(954, 45);
            this.pic_spectrum15.TabIndex = 156;
            this.pic_spectrum15.TabStop = false;
            // 
            // picSpectrumResultantColor14
            // 
            this.picSpectrumResultantColor14.Location = new System.Drawing.Point(1194, 943);
            this.picSpectrumResultantColor14.Name = "picSpectrumResultantColor14";
            this.picSpectrumResultantColor14.Size = new System.Drawing.Size(67, 45);
            this.picSpectrumResultantColor14.TabIndex = 155;
            this.picSpectrumResultantColor14.TabStop = false;
            // 
            // pic_spectrum14
            // 
            this.pic_spectrum14.Location = new System.Drawing.Point(210, 943);
            this.pic_spectrum14.Name = "pic_spectrum14";
            this.pic_spectrum14.Size = new System.Drawing.Size(954, 45);
            this.pic_spectrum14.TabIndex = 154;
            this.pic_spectrum14.TabStop = false;
            // 
            // picSpectrumResultantColor13
            // 
            this.picSpectrumResultantColor13.Location = new System.Drawing.Point(1194, 891);
            this.picSpectrumResultantColor13.Name = "picSpectrumResultantColor13";
            this.picSpectrumResultantColor13.Size = new System.Drawing.Size(67, 45);
            this.picSpectrumResultantColor13.TabIndex = 153;
            this.picSpectrumResultantColor13.TabStop = false;
            // 
            // pic_spectrum13
            // 
            this.pic_spectrum13.Location = new System.Drawing.Point(210, 891);
            this.pic_spectrum13.Name = "pic_spectrum13";
            this.pic_spectrum13.Size = new System.Drawing.Size(954, 45);
            this.pic_spectrum13.TabIndex = 152;
            this.pic_spectrum13.TabStop = false;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Arial", 14F);
            this.label27.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label27.Location = new System.Drawing.Point(14, 120);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(107, 22);
            this.label27.TabIndex = 161;
            this.label27.Text = "Temp State";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Arial", 14F);
            this.label28.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label28.Location = new System.Drawing.Point(57, 156);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(21, 22);
            this.label28.TabIndex = 162;
            this.label28.Text = "0";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Arial", 14F);
            this.label29.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label29.Location = new System.Drawing.Point(57, 218);
            this.label29.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(21, 22);
            this.label29.TabIndex = 163;
            this.label29.Text = "1";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Arial", 14F);
            this.label30.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label30.Location = new System.Drawing.Point(57, 269);
            this.label30.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(21, 22);
            this.label30.TabIndex = 164;
            this.label30.Text = "2";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Arial", 14F);
            this.label31.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label31.Location = new System.Drawing.Point(57, 327);
            this.label31.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(21, 22);
            this.label31.TabIndex = 165;
            this.label31.Text = "3";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Arial", 14F);
            this.label32.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label32.Location = new System.Drawing.Point(57, 549);
            this.label32.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(21, 22);
            this.label32.TabIndex = 169;
            this.label32.Text = "7";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Arial", 14F);
            this.label33.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label33.Location = new System.Drawing.Point(57, 491);
            this.label33.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(21, 22);
            this.label33.TabIndex = 168;
            this.label33.Text = "6";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Arial", 14F);
            this.label34.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label34.Location = new System.Drawing.Point(57, 440);
            this.label34.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(21, 22);
            this.label34.TabIndex = 167;
            this.label34.Text = "5";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Arial", 14F);
            this.label35.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label35.Location = new System.Drawing.Point(57, 378);
            this.label35.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(21, 22);
            this.label35.TabIndex = 166;
            this.label35.Text = "4";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Arial", 14F);
            this.label36.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label36.Location = new System.Drawing.Point(45, 995);
            this.label36.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(32, 22);
            this.label36.TabIndex = 177;
            this.label36.Text = "15";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Arial", 14F);
            this.label37.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label37.Location = new System.Drawing.Point(45, 943);
            this.label37.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(32, 22);
            this.label37.TabIndex = 176;
            this.label37.Text = "14";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Arial", 14F);
            this.label38.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label38.Location = new System.Drawing.Point(46, 885);
            this.label38.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(32, 22);
            this.label38.TabIndex = 175;
            this.label38.Text = "13";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Arial", 14F);
            this.label39.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label39.Location = new System.Drawing.Point(46, 834);
            this.label39.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(32, 22);
            this.label39.TabIndex = 174;
            this.label39.Text = "12";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Arial", 14F);
            this.label40.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label40.Location = new System.Drawing.Point(46, 778);
            this.label40.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(31, 22);
            this.label40.TabIndex = 173;
            this.label40.Text = "11";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Arial", 14F);
            this.label41.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label41.Location = new System.Drawing.Point(46, 731);
            this.label41.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(32, 22);
            this.label41.TabIndex = 172;
            this.label41.Text = "10";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Arial", 14F);
            this.label42.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label42.Location = new System.Drawing.Point(57, 669);
            this.label42.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(21, 22);
            this.label42.TabIndex = 171;
            this.label42.Text = "9";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Arial", 14F);
            this.label43.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label43.Location = new System.Drawing.Point(57, 605);
            this.label43.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(21, 22);
            this.label43.TabIndex = 170;
            this.label43.Text = "8";
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnSave.Font = new System.Drawing.Font("Arial", 12F);
            this.btnSave.Location = new System.Drawing.Point(895, 52);
            this.btnSave.Margin = new System.Windows.Forms.Padding(5);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(98, 61);
            this.btnSave.TabIndex = 178;
            this.btnSave.Text = "Save";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnInsert
            // 
            this.btnInsert.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnInsert.Font = new System.Drawing.Font("Arial", 12F);
            this.btnInsert.Location = new System.Drawing.Point(696, 9);
            this.btnInsert.Margin = new System.Windows.Forms.Padding(5);
            this.btnInsert.Name = "btnInsert";
            this.btnInsert.Size = new System.Drawing.Size(59, 31);
            this.btnInsert.TabIndex = 179;
            this.btnInsert.Text = "Insert";
            this.btnInsert.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnInsert.UseVisualStyleBackColor = false;
            this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
            // 
            // txtInput
            // 
            this.txtInput.Location = new System.Drawing.Point(765, 13);
            this.txtInput.Name = "txtInput";
            this.txtInput.Size = new System.Drawing.Size(228, 20);
            this.txtInput.TabIndex = 180;
            this.txtInput.TextChanged += new System.EventHandler(this.txtInput_TextChanged);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.CornflowerBlue;
            this.button2.Font = new System.Drawing.Font("Arial", 12F);
            this.button2.Location = new System.Drawing.Point(1016, 53);
            this.button2.Margin = new System.Windows.Forms.Padding(5);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(98, 61);
            this.button2.TabIndex = 181;
            this.button2.Text = "Load";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.CornflowerBlue;
            this.button5.Font = new System.Drawing.Font("Arial", 12F);
            this.button5.Location = new System.Drawing.Point(1124, 53);
            this.button5.Margin = new System.Windows.Forms.Padding(5);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(98, 61);
            this.button5.TabIndex = 182;
            this.button5.Text = "Delete";
            this.button5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.CornflowerBlue;
            this.button6.Font = new System.Drawing.Font("Arial", 12F);
            this.button6.Location = new System.Drawing.Point(476, 6);
            this.button6.Margin = new System.Windows.Forms.Padding(5);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(121, 31);
            this.button6.TabIndex = 183;
            this.button6.Text = "Main Menu";
            this.button6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // Spectrum_Editor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(1312, 1063);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.txtInput);
            this.Controls.Add(this.btnInsert);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.label40);
            this.Controls.Add(this.label41);
            this.Controls.Add(this.label42);
            this.Controls.Add(this.label43);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.picSpectrumResultantColor15);
            this.Controls.Add(this.pic_spectrum15);
            this.Controls.Add(this.picSpectrumResultantColor14);
            this.Controls.Add(this.pic_spectrum14);
            this.Controls.Add(this.picSpectrumResultantColor13);
            this.Controls.Add(this.pic_spectrum13);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.picSpectrumResultantColor12);
            this.Controls.Add(this.pic_spectrum12);
            this.Controls.Add(this.picSpectrumResultantColor11);
            this.Controls.Add(this.pic_spectrum11);
            this.Controls.Add(this.picSpectrumResultantColor10);
            this.Controls.Add(this.pic_spectrum10);
            this.Controls.Add(this.picSpectrumResultantColor9);
            this.Controls.Add(this.pic_spectrum9);
            this.Controls.Add(this.picSpectrumResultantColor8);
            this.Controls.Add(this.pic_spectrum8);
            this.Controls.Add(this.picSpectrumResultantColor7);
            this.Controls.Add(this.pic_spectrum7);
            this.Controls.Add(this.picSpectrumResultantColor6);
            this.Controls.Add(this.pic_spectrum6);
            this.Controls.Add(this.picSpectrumResultantColor5);
            this.Controls.Add(this.pic_spectrum5);
            this.Controls.Add(this.picSpectrumResultantColor4);
            this.Controls.Add(this.pic_spectrum4);
            this.Controls.Add(this.picSpectrumResultantColor3);
            this.Controls.Add(this.pic_spectrum3);
            this.Controls.Add(this.picSpectrumResultantColor2);
            this.Controls.Add(this.pic_spectrum2);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.picSpectrumResultantColor1);
            this.Controls.Add(this.pic_spectrum1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbTemperatureState);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.picSpectrumResultantColor);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtIntensity);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btn_λ);
            this.Controls.Add(this.pic_spectrum);
            this.Controls.Add(this.picColorSample);
            this.Controls.Add(this.txt_wavelength_in_nm);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cmbSpectrum);
            this.Name = "Spectrum_Editor";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.Text = "Spectrum_Editor";
            this.Load += new System.EventHandler(this.Spectrum_Editor_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pic_spectrum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picColorSample)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSpectrumResultantColor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSpectrumResultantColor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_spectrum1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSpectrumResultantColor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_spectrum2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSpectrumResultantColor3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_spectrum3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSpectrumResultantColor7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_spectrum7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSpectrumResultantColor6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_spectrum6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSpectrumResultantColor5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_spectrum5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSpectrumResultantColor4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_spectrum4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSpectrumResultantColor12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_spectrum12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSpectrumResultantColor11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_spectrum11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSpectrumResultantColor10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_spectrum10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSpectrumResultantColor9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_spectrum9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSpectrumResultantColor8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_spectrum8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSpectrumResultantColor15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_spectrum15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSpectrumResultantColor14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_spectrum14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSpectrumResultantColor13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_spectrum13)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbSpectrum;
        private System.Windows.Forms.Button btn_λ;
        private System.Windows.Forms.PictureBox pic_spectrum;
        private System.Windows.Forms.PictureBox picColorSample;
        private System.Windows.Forms.TextBox txt_wavelength_in_nm;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtIntensity;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox picSpectrumResultantColor;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.ComboBox cmbTemperatureState;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.PictureBox picSpectrumResultantColor1;
        private System.Windows.Forms.PictureBox pic_spectrum1;
        private System.Windows.Forms.PictureBox picSpectrumResultantColor2;
        private System.Windows.Forms.PictureBox pic_spectrum2;
        private System.Windows.Forms.PictureBox picSpectrumResultantColor3;
        private System.Windows.Forms.PictureBox pic_spectrum3;
        private System.Windows.Forms.PictureBox picSpectrumResultantColor7;
        private System.Windows.Forms.PictureBox pic_spectrum7;
        private System.Windows.Forms.PictureBox picSpectrumResultantColor6;
        private System.Windows.Forms.PictureBox pic_spectrum6;
        private System.Windows.Forms.PictureBox picSpectrumResultantColor5;
        private System.Windows.Forms.PictureBox pic_spectrum5;
        private System.Windows.Forms.PictureBox picSpectrumResultantColor4;
        private System.Windows.Forms.PictureBox pic_spectrum4;
        private System.Windows.Forms.PictureBox picSpectrumResultantColor12;
        private System.Windows.Forms.PictureBox pic_spectrum12;
        private System.Windows.Forms.PictureBox picSpectrumResultantColor11;
        private System.Windows.Forms.PictureBox pic_spectrum11;
        private System.Windows.Forms.PictureBox picSpectrumResultantColor10;
        private System.Windows.Forms.PictureBox pic_spectrum10;
        private System.Windows.Forms.PictureBox picSpectrumResultantColor9;
        private System.Windows.Forms.PictureBox pic_spectrum9;
        private System.Windows.Forms.PictureBox picSpectrumResultantColor8;
        private System.Windows.Forms.PictureBox pic_spectrum8;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.PictureBox picSpectrumResultantColor15;
        private System.Windows.Forms.PictureBox pic_spectrum15;
        private System.Windows.Forms.PictureBox picSpectrumResultantColor14;
        private System.Windows.Forms.PictureBox pic_spectrum14;
        private System.Windows.Forms.PictureBox picSpectrumResultantColor13;
        private System.Windows.Forms.PictureBox pic_spectrum13;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnInsert;
        private System.Windows.Forms.TextBox txtInput;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
    }
}