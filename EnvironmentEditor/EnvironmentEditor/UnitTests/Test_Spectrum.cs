﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EnvironmentEditor
{
    // Tests the spectrum
    class Test_Spectrum
    {
       public static List<string> UnitTest()
        {
            List<string> results = new List<string>();
            Controller control = new Controller();
            results.Add("Testing Spectrum ");
            // 1. verify rows in database
            results.Add("1.verify rows in database");
            System.Data.DataTable dt=new System.Data.DataTable();
            control.GetSpectrum(ref dt);
            if (dt.Rows.Count > 0)
                results.Add("success");
            else results.Add("failure");

           // 2. Validating CSV export/importing and GEt SET methods
            results.Add(" 2. Validating CSV export/importing and GEt SET methods");
            Spectrum s = new Spectrum();
            s.ExportCSV(); 
            s.SetSpectrum(123, 0, 0);
            results.Add("Set Spectrum Value before Import CSV:"+s.GetSpectrum(0, 0));
            s.ImportCSV();
            if(s.GetSpectrum(0,0)==0)
                results.Add("success");
            else results.Add("failure");
            results.Add("Set Spectrum Value After Import CSV:" + s.GetSpectrum(0, 0));


           // 3. Validating database
          results.Add("3.Validating database");
          results.Add(" creating test values");
           for (int i=0;i<16;i++ )
          {
                for(int j=0;j<36;j++)
	            {
                    short intensity =(short)(123*i+j*123);
                    int nm=300+(j*10);
                    results.Add("Temperature State:"+ i  +"Nm  "+nm.ToString() +" Intensity:"+Convert.ToString(intensity));
		            s.SetSpectrum(intensity, nm, i);
	            }
          }
           results.Add(" Loading into database");
           int id = control.ReplaceIntoSpectrum(ref s, "test", -1);
           results.Add(" reading test values");
           control.LoadSpectrum(ref s, id.ToString());
           bool bsuccess = true;
            
           for (int i = 0; i < 16; i++)
           {
               for (int j = 0; j < 36; j++)
               {
                   short bintensity = (short)(123 * i + j * 123);
                   int nm = 300 + (j * 10);
                   int intensity = (short)s.GetSpectrum(nm, i);
                   if (bintensity != intensity)
                       bsuccess = false;
                   results.Add("Temperature State:" + i + "Nm  " + nm.ToString() + " Intensity:" + Convert.ToString(intensity));
               }
           }
           if (bsuccess == true)
               results.Add("success");
           else results.Add("failure");


           // Testing Delete
           results.Add(" ---- Testing Delete -----------------");
           control.DeleteSpectrum(id);

           if (!control.LoadSpectrum(ref s, id.ToString()))
               results.Add("success");
           else results.Add("failure");

            return results;
        }
    }
}
