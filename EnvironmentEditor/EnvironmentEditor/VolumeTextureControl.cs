﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EnvironmentEditor
{
    public partial class VolumeTextureControl : Form
    {
        Add_Image_Cross_Section cross_section;
        public VolumeTextureControl()
        {
            InitializeComponent();
        }

        private void VolumeTextureControl_Load(object sender, EventArgs e)
        {

        }

        private void btnMainMenu_Click(object sender, EventArgs e)
        {
            this.Hide();
            Globals.formsession.Show();
        }

        private void btnAddImage_Click(object sender, EventArgs e)
        {
            cross_section = new Add_Image_Cross_Section();
            cross_section.Show();
        }
    }
}
