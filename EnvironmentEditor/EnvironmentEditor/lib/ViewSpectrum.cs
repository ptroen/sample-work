﻿using System;
using System.Drawing;
// RGB Representation
class ViewSpectrum 
{
    public int Blue;
    public int Green;
    public int Red;
  
    public System.Drawing.Color GetColor()
    {
        System.Drawing.Color col = new System.Drawing.Color();

        return System.Drawing.Color.FromArgb(Red, Green, Blue);
    }

  public System.Drawing.Color  getColorFromWaveLength(int Wavelength,int intensity){
      if (intensity <= 0) { return System.Drawing.Color.FromArgb(0xFF, 0, 0, 0); }
  double Blue;
  double Green;
  double Red;
  if(Wavelength >= 350 && Wavelength <= 439){
   Red	= -(Wavelength - 440.0) / (440.0 - 350.0);
   Green = 0.0;
   Blue	= 1.0;
  }else if(Wavelength >= 440 && Wavelength <= 489){
   Red	= 0.0;
   Green = (Wavelength - 440.0) / (490.0 - 440.0);
   Blue	= 1.0;
  }else if(Wavelength >= 490.0 && Wavelength <= 509){
   Red = 0.0;
   Green = 1.0;
   Blue = -(Wavelength - 510.0) / (510.0 - 490.0);

  }else if(Wavelength >= 510 && Wavelength <= 579){ 
   Red = (Wavelength - 510.0) / (580.0 - 510.0);
   Green = 1.0;
   Blue = 0.0;
  }else if(Wavelength >= 580 && Wavelength <= 644){
   Red = 1.0;
   Green = (645-Wavelength) / (645.0 - 580.0);
   Blue = 0.0;
  }else if(Wavelength >= 645 && Wavelength <= 780){
   Red = 1.0;
   Green = 0.0;
   Blue = 0.0;
  }else{
   Red = 0.0;
   Green = 0.0;
   Blue = 0.0;
  }
  int ii = Math.Abs(intensity);
  double iCarry = 16-Math.Log((double)ii,2);
 iCarry=Math.Max(iCarry, 1);
 iCarry = Math.Min(iCarry, 255);
  this.Red = Convert.ToInt32((Red*255.0)/iCarry);
  this.Green = Convert.ToInt32((Green*255.0) / iCarry);
  this.Blue = Convert.ToInt32((Blue*255.0) / iCarry);
    
  return System.Drawing.Color.FromArgb(0xFF,this.Red, this.Green, this.Blue);
}

  int Log2(int num) {
      int counter = 0;
      while (num != 0)
      {
          num >>= 1;
          counter++;
      }
      return counter;
  }

};