﻿using System.IO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
namespace EnvironmentEditor
{

    class Spectrum : Class_Type_ID
    {
        public Wave[] spectra;
        // public string spectrum_name;

        public Spectrum()
        {
            spectra = new Wave[16];
            for (int i = 0; i < 16; i++)
            {
                spectra[i] = new Wave();
            }
        }

        public void ImportCSV()
        {
            StreamReader reader = new StreamReader("C:\\temp\\spec.csv");
            try
            {
                string header = reader.ReadLine();
                for (int j = 0; j < 16; j++)
                {
                    this.spectra[j].ImportCSV(ref reader);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Message,
                MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void ExportCSV()
        {
            StreamWriter writer = new StreamWriter("C:\\temp\\spec.csv");
            this.spectra[0].ExportCSVHeader(ref writer);
            for (int i = 0; i < 16; i++)
            {
                this.spectra[i].ExportCSV(ref writer);
            }
            writer.Close();
        }

        public void Clear(int state, short intensity)
        {
            this.spectra[state].SetAllWavesConsistent(intensity);
        }
        public int GetMinTempState(int state)
        {
            switch (state)
            {
                case 0: return 0;
                case 1: return 16;
                case 2: return 32;
                case 3: return 64;
                case 4: return 128;
                case 5: return 256;
                case 6: return 512;
                case 7: return 1024;
                case 8: return 2048;
                case 9: return 4096;
                case 10: return 8192;
                case 11: return 16384;
                case 12: return 32768;
                case 13: return 65536;
                case 14: return 131072;
                default: return 262144;
            }
        }

        public int GetTemperatureState(int tempearture_in_K)
        {
            if (tempearture_in_K < 0) return 0;
            else if (tempearture_in_K >= 0 && tempearture_in_K < 16) return 0;
            else if (tempearture_in_K >= 16 && tempearture_in_K < 32) return 1;
            else if (tempearture_in_K >= 32 && tempearture_in_K < 64) return 2;
            else if (tempearture_in_K >= 64 && tempearture_in_K < 128) return 3;
            else if (tempearture_in_K >= 128 && tempearture_in_K < 256) return 4;
            else if (tempearture_in_K >= 256 && tempearture_in_K < 512) return 5;
            else if (tempearture_in_K >= 512 && tempearture_in_K < 1024) return 6;
            else if (tempearture_in_K >= 1024 && tempearture_in_K < 2048) return 7;
            else if (tempearture_in_K >= 2048 && tempearture_in_K < 4096) return 8;
            else if (tempearture_in_K >= 4096 && tempearture_in_K < 8192) return 9;
            else if (tempearture_in_K >= 8192 && tempearture_in_K < 16384) return 10;
            else if (tempearture_in_K >= 16384 && tempearture_in_K < 32768) return 11;
            else if (tempearture_in_K >= 32768 && tempearture_in_K < 65536) return 12;
            else if (tempearture_in_K >= 65536 && tempearture_in_K < 131072) return 13;
            else if (tempearture_in_K >= 131072 && tempearture_in_K < 262144) return 14;
            else if (tempearture_in_K >= 262144 && tempearture_in_K < 524288) return 15;
            else return 15;
        }
        public override int getsize() { return 16 * spectra[0].getsize(); }
        public override int ToByte(ref byte[] data)
        {
            // http://www.dotnetperls.com/buffer
            int offset = 0;
            for (int i = 0; i < 16; i++)
            {
                System.Buffer.BlockCopy(spectra[i].nm, 0, data, offset, spectra[i].getsize());
                offset += spectra[i].getsize();
            }
            return offset;
        }
        public override int SetFromByte(ref  byte[] data,int override_size)
        {
            int offset = 0;
            for (int i = 0; i < 16; i++)
            {
                System.Buffer.BlockCopy(data, offset, spectra[i].nm, 0, spectra[i].getsize());
                offset += spectra[i].getsize();
            }

            return getsize();
        }
        public void DrawSpectraColorSample(ref System.Drawing.Graphics g,
                                                                                     int in_nm,
                                                                                     int tempstate,
                                                                                     short intensity,
                                                                                     int width,
                                                                                     int height)
        {
            Spectrum samplespectrum = new Spectrum();
            //  int tempstate= samplespectrum.GetTemperatureState(temperature_in_K);
            samplespectrum.spectra[tempstate].SetWave(in_nm, intensity);
            // extract the sample color here lots of errors refactoring.....
            System.Drawing.Color rgb = samplespectrum.spectra[tempstate].GetSpectraColor();
            DrawWavelength(ref g, ref rgb, 0, 0, width, height);
        }
        public System.Drawing.Color DrawSpectrum(ref System.Drawing.Graphics g, int temperature_state)
        {
            //   int getindex=GetTemperatureState(passed_in_temperature_in_K);
            System.Drawing.Color rgb = this.spectra[temperature_state].DrawSpectrum(ref g);
            return rgb;
        }
        protected void DrawWavelength(ref System.Drawing.Graphics g, ref System.Drawing.Color rgb, int start_x, int start_y, int width, int height)
        {
            System.Drawing.SolidBrush br = new SolidBrush(rgb);
            g.FillRectangle(br, start_x, start_y, width, height);
        }
        public short GetSpectrum(int in_nm, int temperaturestate) { return this.spectra[temperaturestate].GetWave(in_nm); }
        public void SetSpectrum(short wavelength_intensity, int in_nm, int temperaturestate) { this.spectra[temperaturestate].SetWave(in_nm, wavelength_intensity); }
    };

}