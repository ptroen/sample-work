﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace EnvironmentEditor
{

    class ImageCrossSection : Class_Type_ID
    {
        public float trans_x;
        public float trans_y;
        public float trans_z;
        public UInt32 cross_section_length;
        Texture[] text;

        public ImageCrossSection() { text = new Texture[13];
        cross_section_length = 0;
        }

        /* Converts the obj to a serialized byte structure
         * 
         * 
         * 
         * 
         */
        public override int ToByte(ref byte[] data)
        {
               cross_section_length = 0;
                foreach (var item in text)
	            {
		            cross_section_length=Math.Max(cross_section_length,(UInt32)item.s_height);
	            }
                cross_section_length = Math.Min(cross_section_length, 2048);

            int offset = 0;
            byte[] floatdata = Class_Type_ID.GetBytes(trans_x);
            System.Buffer.BlockCopy(data, offset,floatdata, 0, 4);
            offset += 4;
            
            floatdata = Class_Type_ID.GetBytes(trans_y);
            System.Buffer.BlockCopy(data, offset, floatdata, 0, 4);
            offset += 4;
            
            floatdata = Class_Type_ID.GetBytes(trans_z);
            System.Buffer.BlockCopy(data, offset, floatdata, 0, 4);
            offset += 4;

            floatdata = Class_Type_ID.GetBytes(cross_section_length);
            System.Buffer.BlockCopy(data, offset, floatdata, 0, 4);
            offset += 4;

            for (int i = 0; i < 13; i++)
            {
                int new_offset = 0;
                byte[] data2 = new byte[text[i].getsize()];
                new_offset=text[i].ToByte(ref data2);
                System.Buffer.BlockCopy(data, offset, data2, 0, new_offset);
                offset +=new_offset;
            }

            return offset;
        }
        public override int SetFromByte(ref  byte[] data, int override_size)
        {
            int offset = 0;

            int size = override_size;
            // we can use the fact that the channels have a certain width and share a common height to decode this big thingy
            // they must have the same height or it will break...

            this.cross_section_length = Class_Type_ID.ToUInt32(data, offset);

            this.trans_x = Class_Type_ID.ToSingle(data, offset);
            offset += 4;
            this.trans_y = Class_Type_ID.ToSingle(data, offset);
            offset += 4;
            this.trans_z= Class_Type_ID.ToSingle(data, offset);
            offset += 4;

            for (int i = 0; i < 13; i++)
            {
                int new_offset = 0;
                byte[] texture_page;
                int size_to_allocate = ((int)(cross_section_length * cross_section_length)) + this.text[i].GetNonImage_size();
                texture_page=new byte[size_to_allocate];

                System.Buffer.BlockCopy(data, offset, texture_page, 0, size_to_allocate);

                this.text[i].SetFromByte(ref texture_page,size_to_allocate);
                offset += new_offset;
            }
            return offset;
        }

        public override int getsize()
        {
            int offset;
            offset = 0;
            offset += sizeof(float) * 3;
            for (int i = 0; i < 13; i++)
            {
                offset += text[i].getsize();
            }
            return offset;
        }

        public Texture GetEmission() { return text[0]; }
        public Texture GetAbsorption() { return text[1]; }
        public Texture GetEmissionDepth() { return text[2]; }
        public Texture GetAbsorptionDepth() { return text[3]; }
        public Texture GetAbsorptionNormalX() { return text[4]; }
        public Texture GetAbsorptionNormalY() { return text[5]; }
        public Texture GetAbsorptionNormalZ() { return text[6]; }
        public Texture GetTransparencyLightScatteringChannel() { return text[7]; }
        public Texture GetEmissionTemperature() { return text[8]; }
        public Texture GetAbsorptionTemperature() { return text[9]; }
        public Texture GetLightScatteringNormalX() { return text[10]; }
        public Texture GetLightScatteringNormalY() { return text[11]; }
        public Texture GetLightScatteringNormalZ() { return text[12]; }

        public void SetEmission(ref Texture stext) {text[0] = stext;  }
        public void SetAbsorption(ref Texture stext) { text[1] = stext; }
        public void SetEmissionDepth(ref Texture stext) { text[2] = stext; }
        public void SetAbsorptionDepth(ref Texture stext) { text[3] = stext; }
        public void SetAbsorptionNormalX(ref Texture stext) { text[4] = stext; }
        public void SetAbsorptionNormalY(ref Texture stext) { text[5] = stext; }
        public void SetAbsorptionNormalZ(ref Texture stext) { text[6] = stext; }
        public void SetTransparencyLightScatteringChannel(ref Texture stext) { text[7] = stext; }
        public void SetEmissionTemperature(ref Texture stext) { text[8] = stext; }
        public void SetAbsorptionTemperature(ref Texture stext) { text[9] = stext; }
        public void SetLightScatteringNormalX(ref Texture stext) { text[10] = stext; }
        public void SetLightScatteringNormalY(ref Texture stext) { text[11] = stext; }
        public void SetLightScatteringNormalZ(ref Texture stext) { text[12] = stext; }


    }
}
