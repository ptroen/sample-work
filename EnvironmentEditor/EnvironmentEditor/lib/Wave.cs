﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace EnvironmentEditor
{
    class Wave : Class_Type_ID
    {

        public short[] nm;
        public Wave()
        {
            nm = new short[36];

        }



        public override int getsize()
        {
            // http://msdn.microsoft.com/en-us/library/y3ybkfb3.aspx
            return sizeof(short) * 36;
        }
        public override int ToByte(ref byte[] data)
        {
            System.Buffer.BlockCopy(nm, 0, data, 0, getsize());
            return getsize();
        }

        public override int SetFromByte(ref  byte[] data, int override_size) {
            System.Buffer.BlockCopy(data, 0, nm, 0, getsize());
            return getsize();
        }

        public void ImportCSV(ref StreamReader reader)
        {
            try
            {
                string line = reader.ReadLine();
                string[] wavelengths = line.Split(',');
                for (int i = 0; i < 36; i++)
                    nm[i] = Convert.ToInt16(wavelengths[i]);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, ex.Message,
                MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public short GetWave(int wavelength)
        {
            if (wavelength <= 300) return this.nm[0];
            if (wavelength >= 650) return this.nm[35];
            try
            {
                int wave = wavelength;
                wave = wave - (wave % 10);
                wavelength = (short)wave;
            }
            catch (Exception ex)
            {
                return this.nm[0];
            }
            switch (wavelength)
            {
                case 300: return this.nm[0];
                case 310: return this.nm[1];
                case 320: return this.nm[2];
                case 330: return this.nm[3];
                case 340: return this.nm[4];
                case 350: return this.nm[5];
                case 360: return this.nm[6];
                case 370: return this.nm[7];
                case 380: return this.nm[8];
                case 390: return this.nm[9];
                case 400: return this.nm[10];
                case 410: return this.nm[11];
                case 420: return this.nm[12];
                case 430: return this.nm[13];
                case 440: return this.nm[14];
                case 450: return this.nm[15];
                case 460: return this.nm[16];
                case 470: return this.nm[17];
                case 480: return this.nm[18];
                case 490: return this.nm[19];
                case 500: return this.nm[20];
                case 510: return this.nm[21];
                case 520: return this.nm[22];
                case 530: return this.nm[23];
                case 540: return this.nm[24];
                case 550: return this.nm[25];
                case 560: return this.nm[26];
                case 570: return this.nm[27];
                case 580: return this.nm[28];
                case 590: return this.nm[29];
                case 600: return this.nm[30];
                case 610: return this.nm[31];
                case 620: return this.nm[32];
                case 630: return this.nm[33];
                case 640: return this.nm[34];
                case 650: return this.nm[35];
                default: return nm[35];
            }

        }

        public void SetAllWavesConsistent(short intensity)
        {
            for (int i = 300; i < 660; i += 10) SetWave(i, intensity);

        }
        public void SetWave(int wavelength, short intensity)
        {
            if (wavelength <= 300)
            {
                this.nm[0] = (short)intensity;
                return;
            }
            if (wavelength >= 650)
            {
                this.nm[35] = (short)intensity;
                return;
            }
            int wave = wavelength;
            try
            {

                wave = wave - (wave % 10);
                wavelength = (short)wave;
            }
            catch (Exception ex)
            {

            }
            switch (wave)
            {
                case 300: this.nm[0] = intensity; break;
                case 310: this.nm[1] = intensity; break;
                case 320: this.nm[2] = intensity; break;
                case 330: this.nm[3] = intensity; break;
                case 340: this.nm[4] = intensity; break;
                case 350: this.nm[5] = intensity; break;
                case 360: this.nm[6] = intensity; break;
                case 370: this.nm[7] = intensity; break;
                case 380: this.nm[8] = intensity; break;
                case 390: this.nm[9] = intensity; break;
                case 400: this.nm[10] = intensity; break;
                case 410: this.nm[11] = intensity; break;
                case 420: this.nm[12] = intensity; break;
                case 430: this.nm[13] = intensity; break;
                case 440: this.nm[14] = intensity; break;
                case 450: this.nm[15] = intensity; break;
                case 460: this.nm[16] = intensity; break;
                case 470: this.nm[17] = intensity; break;
                case 480: this.nm[18] = intensity; break;
                case 490: this.nm[19] = intensity; break;
                case 500: this.nm[20] = intensity; break;
                case 510: this.nm[21] = intensity; break;
                case 520: this.nm[22] = intensity; break;
                case 530: this.nm[23] = intensity; break;
                case 540: this.nm[24] = intensity; break;
                case 550: this.nm[25] = intensity; break;
                case 560: this.nm[26] = intensity; break;
                case 570: this.nm[27] = intensity; break;
                case 580: this.nm[28] = intensity; break;
                case 590: this.nm[29] = intensity; break;
                case 600: this.nm[30] = intensity; break;
                case 610: this.nm[31] = intensity; break;
                case 620: this.nm[32] = intensity; break;
                case 630: this.nm[33] = intensity; break;
                case 640: this.nm[34] = intensity; break;
                case 650: this.nm[35] = intensity; break;
            }

        }

        public void ExportCSV(ref StreamWriter writer)
        {
            for (int i = 0; i < 36; i++)
            {
                writer.Write(Convert.ToString(this.nm[i]));
                writer.Write(",");
            }
            writer.Write("\n");
        }

        public void ExportCSVHeader(ref StreamWriter writer)
        {
            for (int i = 0; i < 36; i++)
            {
                int temp = 300;
                temp += i * 10;
                writer.Write(Convert.ToString(temp) + "nm\0");
                writer.Write(",");
            }
            writer.Write("\n");
        }

        public System.Drawing.Color DrawSpectrum(ref System.Drawing.Graphics g)
        {
            ViewSpectrum view = new ViewSpectrum();
            for (int i = 0; i < 36; i++)
            {
                System.Drawing.Color rgb = view.getColorFromWaveLength(((i * 10) + 300), this.nm[i]);
                int start_x = i * 20;
                int width = 20;
                int start_y = 32;
                // converting b the intensity
                double d = Convert.ToDouble(this.nm[i]) / 32767.0;
                int height = Convert.ToInt32(d * 32); // max is 128 min is -127
                if (this.nm[i] >= 0) ++height;
                System.Drawing.SolidBrush br = new SolidBrush(rgb);
                if (height >= 0)
                {
                    g.FillRectangle(br, start_x, start_y - height, width, height);
                }
                else
                {
                    g.FillRectangle(br, start_x, start_y - height, width, -1 * height);
                }
            }
            return GetSpectraColor();
        }
        // gets only the individual wave not the whole spectrum
        public static System.Drawing.Color GetSpectraColorSample(int wavelength, int intensity)
        {
            ViewSpectrum view = new ViewSpectrum();
            return view.getColorFromWaveLength(wavelength, intensity);
        }

        public System.Drawing.Color GetSpectraColor()
        {
            System.Drawing.Color rgb;
            ViewSpectrum view = new ViewSpectrum();
            int red, green, blue;
            red = 0; blue = 0; green = 0;
            int rCount = 0, gCount = 0, bCount = 0;
            red = 0;
            green = 0;
            blue = 0;
            for (int i = 0; i < 36; i++)
            {
                short intensity = this.nm[i];
                rgb = view.getColorFromWaveLength((i * 10) + 300, intensity);
                // ensure we aren't adding black
                if (rgb.R > 0)
                {
                    red += rgb.R;
                    ++rCount;
                }
                if (rgb.B > 0)
                {
                    blue += rgb.B;
                    ++bCount;
                }
                if (rgb.G > 0)
                {
                    green += rgb.G;
                    ++gCount;
                }
            }
            if (rCount > 0) red /= rCount;
            if (gCount > 0) green /= gCount;
            if (bCount > 0) blue /= bCount;
            return System.Drawing.Color.FromArgb(0xff, red, green, blue);
        }

    }


}
