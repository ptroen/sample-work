﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Windows.Forms;
using System.Text;
namespace EnvironmentEditor
{
    class SQLiteDatabase
    {
        String dbConnection;

        /// <summary>
        ///     Default Constructor for SQLiteDatabase Class.
        /// </summary>
        public SQLiteDatabase(int db_state = 0)
        {
            if (db_state == 1) { dbConnection = "Data Source=C:\\rtsengine\\Image.db"; }
            else
                dbConnection = "Data Source=C:\\rtsengine\\game.db";
        }

        /// <summary>
        ///     Single Param Constructor for specifying the DB file.
        /// </summary>
        /// <param name="inputFile">The File containing the DB</param>
        public SQLiteDatabase(String inputFile)
        {
            dbConnection = String.Format("Data Source={0}", inputFile);
        }

        /// <summary>
        ///     Single Param Constructor for specifying advanced connection options.
        /// </summary>
        /// <param name="connectionOpts">A dictionary containing all desired options and their values</param>
        public SQLiteDatabase(Dictionary<String, String> connectionOpts)
        {
            String str = "";
            foreach (KeyValuePair<String, String> row in connectionOpts)
            {
                str += String.Format("{0}={1}; ", row.Key, row.Value);
            }
            str = str.Trim().Substring(0, str.Length - 1);
            dbConnection = str;
        }

        /// <summary>
        ///     Allows the programmer to run a query against the Database.
        /// </summary>
        /// <param name="sql">The SQL to run</param>
        /// <returns>A DataTable containing the result set.</returns>
        public DataTable GetDataTable(string sql)
        {
            DataTable dt = new DataTable();
            try
            {
                SQLiteConnection cnn = new SQLiteConnection(dbConnection);
                cnn.Open();
                SQLiteCommand mycommand = new SQLiteCommand(cnn);
                mycommand.CommandText = sql;
                SQLiteDataReader reader = mycommand.ExecuteReader();
                dt.Load(reader);
                reader.Close();
                cnn.Close();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return dt;
        }

        /// <summary>
        ///     Allows the programmer to interact with the database for purposes other than a query.
        /// </summary>
        /// <param name="sql">The SQL to be run.</param>
        /// <returns>An Integer containing the number of rows updated.</returns>
        public int ExecuteNonQuery(string sql)
        {
            SQLiteConnection cnn = new SQLiteConnection(dbConnection);
            cnn.Open();
            SQLiteCommand mycommand = new SQLiteCommand(cnn);
            mycommand.CommandText = sql;
            int rowsUpdated = mycommand.ExecuteNonQuery();
            cnn.Close();
            return rowsUpdated;
        }

        /// <summary>
        ///     Allows the programmer to retrieve single items from the DB.
        /// </summary>
        /// <param name="sql">The query to run.</param>
        /// <returns>A string.</returns>
        public string ExecuteScalar(string sql)
        {
            SQLiteConnection cnn = new SQLiteConnection(dbConnection);
            cnn.Open();
            SQLiteCommand mycommand = new SQLiteCommand(cnn);
            mycommand.CommandText = sql;
            object value = mycommand.ExecuteScalar();
            cnn.Close();
            if (value != null)
            {
                return value.ToString();
            }
            return "";
        }
        /// <summary>
        ///     Allows the programmer to easily delete rows from the DB.
        /// </summary>
        /// <param name="tableName">The table from which to delete.</param>
        /// <param name="where">The where clause for the delete.</param>
        /// <returns>A boolean true or false to signify success or failure.</returns>
        public bool Delete(String tableName, String wherecol, string wherecond)
        {
            Boolean returnCode = true;
            try
            {
                string sql = "insert or replace into " + tableName + " ( del ) Values(1) where " + wherecol + "='" + wherecond + "'";
                int result = ExecuteNonQuery(sql);
            }
            catch (Exception fail)
            {
                MessageBox.Show(fail.Message);
                returnCode = false;
            }
            return returnCode;
        }

        public int find(String tableName, ref String name)
        {
            try
            {
                string sql = "select id from " + tableName + " where name=" + name;
                DataTable dt = GetDataTable(sql);

                DataRow r = dt.Rows[0];
                return Convert.ToInt32(r[0]);
            }
            catch (System.Exception ex)
            {
                return 1;
            }

        }

        /// <summary>
        ///     Allows the programmer to easily insert into the DB
        /// </summary>
        /// <param name="tableName">The table into which we insert the data.</param>
        /// <param name="data">A dictionary containing the column names and data for the insert.</param>
        /// <returns>A boolean true or false to signify success or failure.</returns>
        public bool ReplaceInto(String tableName, Dictionary<String, String> data)
        {
            String columns = "";
            String values = "";
            Boolean returnCode = true;
            foreach (KeyValuePair<String, String> val in data)
            {
                columns += String.Format(" {0},", val.Key.ToString());
                values += String.Format(" '{0}',", val.Value);
            }
            columns = columns.Substring(0, columns.Length - 1);
            values = values.Substring(0, values.Length - 1);
            try
            {
                string sql = String.Format("insert or replace into {0}({1}) values({2});", tableName, columns, values);
                this.ExecuteNonQuery(sql);
            }
            catch (Exception fail)
            {
                MessageBox.Show(fail.Message);
                returnCode = false;
            }
            return returnCode;
        }

        public void ReplaceIntoBlob(String tableName,
                                                           ref Dictionary<String, String> classstrings,
                                                            ref Dictionary<String, Class_Type_ID> blob)
        {
            string sql_header = "INSERT OR REPLACE INTO " + tableName;
            string values = "";
            string columns = "";
            int i = 0;
            foreach (KeyValuePair<String, String> val in classstrings)
            {
                if (i > 0) columns += ",";
                columns += val.Key.ToString();

                if (i > 0) values += ",";
                values += "'" + val.Value + "'";
                ++i;
            }
            foreach (KeyValuePair<String, Class_Type_ID> val in blob)
            {
                // first just the keys
                if (i > 0) columns += ",";
                columns += val.Key.ToString();
                if (i > 0) values += ",";
                values += "@" + val.Key.ToString();
                ++i;
            }

            string sql = sql_header + " (" + columns + ") VALUES (" + values + ")";
            var connection = new SQLiteConnection(dbConnection);
            connection.Open();
            var command = new SQLiteCommand(sql, connection);
            foreach (KeyValuePair<String, Class_Type_ID> c in blob)
            {
                byte[] data = new Byte[c.Value.getsize()];
                c.Value.ToByte(ref data);
                int size = c.Value.getsize();
                command.Parameters.Add("@" + c.Key.ToString() + "", DbType.Binary, size).Value = data;
            }

            command.ExecuteNonQuery();
            connection.Close();
        }

        // SelectBlob gets column name and blob value
        // citations
        // http://fredgan.wordpress.com/2008/08/01/read-write-blobs-from-to-sqlite-using-c-net-datareader/
        // http://stackoverflow.com/questions/625029/how-do-i-store-and-retrieve-a-blob-from-sqlite for reader.GetBytes
        public bool SelectBlob(String tableName,
                                                ref Dictionary<String, String> classstrings,
                                                ref Dictionary<String, Class_Type_ID> classblobs,
                                                 string column_wherename,
                                                string column_wherevalue)
        {

            string sql_column = "";
            int i = 0;

            // populate the columns
            foreach (KeyValuePair<String, String> val in classstrings)
            {
                if (i > 0) sql_column += ',';
                sql_column += val.Key;
                ++i;
            }
            foreach (KeyValuePair<String, Class_Type_ID> col in classblobs)
            {
                if (i > 0) sql_column += ',';
                sql_column += col.Key;
                ++i;
            }
            // end populating the columns

            // construct the sql and read the query
            try
            {
                var connection = new SQLiteConnection(dbConnection);
                connection.Open();
                string sql = "SELECT " + sql_column + " FROM " + tableName + " WHERE " + column_wherename + " = '" + column_wherevalue + "'";
                var command = new SQLiteCommand(sql, connection);


                SQLiteDataReader reader = command.ExecuteReader();
                // for the reader
                i = 0;
                bool read = false;
                // reading in the columns
                while (reader.Read())
                {
                    read = true;
                    // read in all the string keys
                    foreach (KeyValuePair<String, String> val in classstrings)
                    {
                        classstrings[val.Key] = reader[i].ToString();
                        ++i;
                    }
                    foreach (KeyValuePair<String, Class_Type_ID> col in classblobs)
                    {
                        byte[] data = new byte[col.Value.getsize()];
                        reader.GetBytes(i, 0, data, 0, col.Value.getsize());
                        classblobs[col.Key].SetFromByte(ref data,-1);
                        ++i;
                    }

                }
                connection.Close();
                return read;
            }
            catch (System.Exception ex)
            {
                return false;
            }
        }


        /// </summary>
        /// <returns>A boolean true or false to signify success or failure.</returns>
        public bool ClearDB()
        {
            DataTable tables;
            try
            {
                tables = this.GetDataTable("select NAME from SQLITE_MASTER where type='table' order by NAME;");
                foreach (DataRow table in tables.Rows)
                {
                    this.ClearTable(table["NAME"].ToString());
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        ///     Allows the user to easily clear all data from a specific table.
        /// </summary>
        /// <param name="table">The name of the table to clear.</param>
        /// <returns>A boolean true or false to signify success or failure.</returns>
        public bool ClearTable(String table)
        {
            try
            {

                this.ExecuteNonQuery(String.Format("delete from {0};", table));
                return true;
            }
            catch
            {
                return false;
            }
        }
    }

}