﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
namespace EnvironmentEditor
{
    class Texture :  Class_Type_ID
    {
         short number_of_channels;
        public  int s_height; // we only allow square textures
         byte[] image_array1;
         byte[] image_array2;
         byte[] image_array3;
         byte[] image_array4;

         public Texture()
         {
             image_array1 = new byte[2048 * 2048];
             image_array2 = new byte[2048 * 2048];
             image_array3 = new byte[2048 * 2048];
             image_array4 = new byte[2048 * 2048];
         }

         protected void SetArray(int offset,int width, int height,byte byte_to_set)
         {
             switch (offset)
             {
                 case 1: image_array1[ (s_height * height) + width] = byte_to_set; return;
                 case 2: image_array2[(s_height * height) + width] = byte_to_set; return;
                 case 3: image_array3[ (s_height * height) + width] = byte_to_set; return;
                 case 4: image_array4[ (s_height * height) + width] = byte_to_set; return;
             }
            // image_array[offset, (s_height * height) + width] = byte_to_set;
         }

         public int GetNonImage_size()
         {
             int size = 0;
             size += sizeof(short);
             size += sizeof(int);

             return size;
         }

         public override int getsize()
         {
             int size = 0;
             size += GetNonImage_size();
             size += number_of_channels*s_height*s_height;
             return size;
         }
         // use toByte to pass back and forth not operator=
         public override int ToByte(ref byte[] data)
         {
             int offset = 0;
             byte[] floatdata = Class_Type_ID.GetBytes(number_of_channels);
             System.Buffer.BlockCopy(data, offset, floatdata, 0, 4);
             offset += 2;

             floatdata = Class_Type_ID.GetBytes(s_height);
             System.Buffer.BlockCopy(data, offset, floatdata, 0, 4);
             offset += 4;

             if(number_of_channels>=4){
                 System.Buffer.BlockCopy(data, offset, image_array4, 0, s_height * s_height);
             }

             if (number_of_channels >= 3)
             {
                 System.Buffer.BlockCopy(data, offset, image_array3, 0, s_height * s_height);
             }

             if (number_of_channels >= 2)
             {
                 System.Buffer.BlockCopy(data, offset, image_array2, 0, s_height * s_height);
             }

             if (number_of_channels >= 1)
             {
                 System.Buffer.BlockCopy(data, offset, image_array1, 0, s_height * s_height);
             }
        
             return offset;
         }
         public override int SetFromByte(ref  byte[] data,int override_size)
         {

             return getsize();
         }

        public Texture(ref System.Windows.Forms.PictureBox pic,
                        short s_number_of_channels=4){

                            image_array1 = new byte[2048 * 2048];
                            image_array2 = new byte[2048 * 2048];
                            image_array3 = new byte[2048 * 2048];
                            image_array4 = new byte[2048 * 2048]; 

                            number_of_channels=s_number_of_channels;

                            Bitmap bmp = (Bitmap)pic.Image;
                            int width = bmp.Width;
                            int height = bmp.Height;
                            s_height =  height;
                            for (int x = 0; x < width; x++)
                                for (int y = 0; y < height; y++)
                                {
                                    int R = 0, G = 0, B = 0, A = 0;
                                    Color c = bmp.GetPixel(x, y);
                                    if (number_of_channels>=1)
                                    {
                                        R = c.R;
                                        SetArray(0, width, height, c.R);
                                    }

                                    if (number_of_channels >= 2)
                                    {
                                        SetArray(1, width, height, c.G);
                                        G = c.G;
                                    }
                                    if (number_of_channels >= 3)
                                    {
                                        B = c.B;
                                        SetArray(2, width, height, c.B);

                                    }
                                    if (number_of_channels >= 4)
                                    {
                                        A = c.A;
                                        SetArray(3, width, height, c.A);
                                    }   
                                     Color dest = Color.FromArgb(A, R, G, B);
                                    bmp.SetPixel(x, y, dest);
                                }
                            pic.Image = bmp;
        }


        public static Texture process_texture(   ref System.Windows.Forms.PictureBox pic, 
                                                                        short s_number_of_channels = 4  )
        {
            return new Texture(ref pic, s_number_of_channels);
         }
    }
}