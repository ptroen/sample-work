﻿namespace EnvironmentEditor
{
    public abstract class Class_Type_ID
    {
        public abstract int getsize();
        // use toByte to pass back and forth not operator=
        // returns offset
        public abstract int ToByte(ref byte[] data);
        
        // return back the number of bytes set
        public abstract int SetFromByte(ref  byte[] data,int override_size);

        ulong GetMurmurHash3()
        {
            string s = "";
            byte[] data = new byte[this.getsize()];
            this.ToByte(ref data);
            Murmur3 m = new Murmur3();
            byte[] hash = m.ComputeHash(data);
            return IntHelpers.GetUInt64(hash, 0);
        }

        protected static byte[] GetBytes(uint value)
        {
            return new byte[4] { 
                    (byte)(value & 0xFF), 
                    (byte)((value >> 8) & 0xFF), 
                    (byte)((value >> 16) & 0xFF), 
                    (byte)((value >> 24) & 0xFF) };
        }

        protected static byte[] GetBytes(short value)
        {
            return new byte[2] { 
                    (byte)(value & 0xFF), 
                    (byte)((value >> 8) & 0xFF) };
        }

        protected static unsafe byte[] GetBytes(float value)
        {
            uint val = *((uint*)&value);
            return GetBytes(val);
        }

        protected static unsafe byte[] GetBytes(float value, ByteOrder order)
        {
            byte[] bytes = GetBytes(value);
            if (order != ByteOrder.LittleEndian)
            {
                System.Array.Reverse(bytes);
            }
            return bytes;
        }

        protected static ulong ToUInt64(byte[] value, int index)
        {
            return (uint)(
                value[0 + index] << 0 |
                value[1 + index] << 8 |
                value[2 + index] << 16 |
                value[3 + index] << 24 |
                value[0 + index] << 32 |
                value[1 + index] << 40 |
                value[2 + index] << 48 |
                value[3 + index] << 56);
        }

        protected static uint ToUInt32(byte[] value, int index)
        {
            return (uint)(
                value[0 + index] << 0 |
                value[1 + index] << 8 |
                value[2 + index] << 16 |
                value[3 + index] << 24);
        }

        protected static unsafe double ToDouble(byte[] value, int index)
        {
            ulong i = ToUInt64(value, index);
            return *(((double*)&i));
        }

        protected static unsafe float ToSingle(byte[] value, int index)
        {
            uint i = ToUInt32(value, index);
            return *(((float*)&i));
        }

        protected static unsafe float ToSingle(byte[] value, int index, ByteOrder order)
        {
            if (order != ByteOrder.LittleEndian)
            {
                System.Array.Reverse(value, index, value.Length);
            }
            return ToSingle(value, index);
        }

        protected static unsafe double ToDouble(byte[] value, int index, ByteOrder order)
        {
            if (order != ByteOrder.LittleEndian)
            {
                System.Array.Reverse(value, index, value.Length);
            }
            return ToDouble(value, index);
        }

        public enum ByteOrder
        {
            LittleEndian,
            BigEndian
        }

        static protected bool IsLittleEndian
        {
            get
            {
                unsafe
                {
                    int i = 1;
                    char* p = (char*)&i;

                    return (p[0] == 1);
                }
            }
        }


    };
}