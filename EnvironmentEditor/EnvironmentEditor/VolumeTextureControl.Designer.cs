﻿namespace EnvironmentEditor
{
    partial class VolumeTextureControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnMainMenu = new System.Windows.Forms.Button();
            this.btnAddImage = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.btnRemoveCrossSection = new System.Windows.Forms.Button();
            this.cmb = new System.Windows.Forms.ComboBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnLoad = new System.Windows.Forms.Button();
            this.label27 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtInput = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.cmbCrossSections = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // btnMainMenu
            // 
            this.btnMainMenu.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnMainMenu.Font = new System.Drawing.Font("Arial", 12F);
            this.btnMainMenu.Location = new System.Drawing.Point(14, 2);
            this.btnMainMenu.Margin = new System.Windows.Forms.Padding(5);
            this.btnMainMenu.Name = "btnMainMenu";
            this.btnMainMenu.Size = new System.Drawing.Size(186, 53);
            this.btnMainMenu.TabIndex = 66;
            this.btnMainMenu.Text = "Main Menu";
            this.btnMainMenu.UseVisualStyleBackColor = false;
            this.btnMainMenu.Click += new System.EventHandler(this.btnMainMenu_Click);
            // 
            // btnAddImage
            // 
            this.btnAddImage.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnAddImage.Font = new System.Drawing.Font("Arial", 12F);
            this.btnAddImage.Location = new System.Drawing.Point(14, 79);
            this.btnAddImage.Margin = new System.Windows.Forms.Padding(5);
            this.btnAddImage.Name = "btnAddImage";
            this.btnAddImage.Size = new System.Drawing.Size(186, 53);
            this.btnAddImage.TabIndex = 68;
            this.btnAddImage.Text = "Add Image Cross Section";
            this.btnAddImage.UseVisualStyleBackColor = false;
            this.btnAddImage.Click += new System.EventHandler(this.btnAddImage_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.CornflowerBlue;
            this.button4.Font = new System.Drawing.Font("Arial", 12F);
            this.button4.Location = new System.Drawing.Point(35, 491);
            this.button4.Margin = new System.Windows.Forms.Padding(5);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(1053, 46);
            this.button4.TabIndex = 70;
            this.button4.Text = "Process Cross Sections and Build the Final Volumetric Images";
            this.button4.UseVisualStyleBackColor = false;
            // 
            // btnRemoveCrossSection
            // 
            this.btnRemoveCrossSection.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnRemoveCrossSection.Font = new System.Drawing.Font("Arial", 12F);
            this.btnRemoveCrossSection.Location = new System.Drawing.Point(210, 79);
            this.btnRemoveCrossSection.Margin = new System.Windows.Forms.Padding(5);
            this.btnRemoveCrossSection.Name = "btnRemoveCrossSection";
            this.btnRemoveCrossSection.Size = new System.Drawing.Size(186, 53);
            this.btnRemoveCrossSection.TabIndex = 71;
            this.btnRemoveCrossSection.Text = "Remove Cross Section";
            this.btnRemoveCrossSection.UseVisualStyleBackColor = false;
            // 
            // cmb
            // 
            this.cmb.FormattingEnabled = true;
            this.cmb.Location = new System.Drawing.Point(277, 26);
            this.cmb.Name = "cmb";
            this.cmb.Size = new System.Drawing.Size(385, 21);
            this.cmb.TabIndex = 72;
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnSave.Font = new System.Drawing.Font("Arial", 12F);
            this.btnSave.Location = new System.Drawing.Point(718, 14);
            this.btnSave.Margin = new System.Windows.Forms.Padding(5);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(182, 41);
            this.btnSave.TabIndex = 73;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            // 
            // btnLoad
            // 
            this.btnLoad.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnLoad.Font = new System.Drawing.Font("Arial", 12F);
            this.btnLoad.Location = new System.Drawing.Point(906, 14);
            this.btnLoad.Margin = new System.Windows.Forms.Padding(5);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(182, 41);
            this.btnLoad.TabIndex = 74;
            this.btnLoad.Text = "Load";
            this.btnLoad.UseVisualStyleBackColor = false;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Arial", 14F);
            this.label27.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label27.Location = new System.Drawing.Point(45, 234);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(61, 22);
            this.label27.TabIndex = 162;
            this.label27.Text = "Depth";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 14F);
            this.label1.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label1.Location = new System.Drawing.Point(45, 197);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(134, 22);
            this.label1.TabIndex = 163;
            this.label1.Text = "Height( in tiles)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 14F);
            this.label2.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label2.Location = new System.Drawing.Point(45, 163);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(195, 22);
            this.label2.TabIndex = 164;
            this.label2.Text = "Width(in 16 pixel tiles)";
            // 
            // txtInput
            // 
            this.txtInput.Location = new System.Drawing.Point(289, 238);
            this.txtInput.Name = "txtInput";
            this.txtInput.Size = new System.Drawing.Size(107, 20);
            this.txtInput.TabIndex = 181;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(289, 201);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(107, 20);
            this.textBox1.TabIndex = 182;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(289, 163);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(107, 20);
            this.textBox2.TabIndex = 183;
            // 
            // cmbCrossSections
            // 
            this.cmbCrossSections.FormattingEnabled = true;
            this.cmbCrossSections.Location = new System.Drawing.Point(423, 97);
            this.cmbCrossSections.Name = "cmbCrossSections";
            this.cmbCrossSections.Size = new System.Drawing.Size(385, 21);
            this.cmbCrossSections.TabIndex = 184;
            // 
            // VolumeTextureControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(1184, 551);
            this.Controls.Add(this.cmbCrossSections);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.txtInput);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.btnLoad);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.cmb);
            this.Controls.Add(this.btnRemoveCrossSection);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.btnAddImage);
            this.Controls.Add(this.btnMainMenu);
            this.Name = "VolumeTextureControl";
            this.Text = "VolumeTextureControl";
            this.Load += new System.EventHandler(this.VolumeTextureControl_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnMainMenu;
        private System.Windows.Forms.Button btnAddImage;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button btnRemoveCrossSection;
        private System.Windows.Forms.ComboBox cmb;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtInput;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.ComboBox cmbCrossSections;
    }
}