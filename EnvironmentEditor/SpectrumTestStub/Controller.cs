﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SQLite;
namespace EnvironmentEditor
{
    class Controller
    {
        SQLiteDatabase db;
        public Controller()
        {
          db=new SQLiteDatabase();
        }

        public void GetSession(ref DataTable dt)
        {
            string s = "SELECT *  from Session_Name";
            dt = db.GetDataTable(s);
        }

        public void ReplaceIntoSession( ref string sessionname)
        {
            string table = "Session_Name";
            Dictionary<String, String> data=new Dictionary<String, String>();
            data.Add("session_name",sessionname);
            db.ReplaceInto(table, data);
        }

        public void LoadSpectrum(ref Spectrum s,string id)
        {
            Dictionary<String, String> classstrings=new Dictionary<String, String>();
            Dictionary<String, Class_Type_ID> classblobs=new Dictionary<String, Class_Type_ID>();
            classblobs.Add("spec", s);
         
            // =
            byte[] data = new byte[classblobs["spec"].getsize()];
            s.ToByte(ref data);
            classblobs["spec"].SetFromByte(ref data);
            
            // queries
            db.SelectBlob("spectrum",
                                            ref  classstrings,
                                            ref  classblobs,
                                             ("id"),
                                            id);

            // =
             data = new byte[classblobs["spec"].getsize()];
            classblobs["spec"].ToByte(ref data);
            s.SetFromByte(ref data);
        }

    public void GetSpectrum(ref DataTable dt)
        {
            string s = "SELECT *  from spectrum";
            dt = db.GetDataTable(s);
        }

    private int GetMaxIDFromSpectrum() {
        string sql = "select max(id)  from spectrum";
           DataTable dt=db.GetDataTable(sql);
             DataRow r = dt.Rows[0];
        return Convert.ToInt32(r[0])+1;

    }

        // http://msdn.microsoft.com/en-us/library/4f5s1we0%28v=vs.71%29.aspx
        public  int ReplaceIntoSpectrum(ref Spectrum spec,string spectrumname,int id)
        {
            string table = "spectrum";
            Dictionary<String, String> data=new Dictionary<String, String>();
            Dictionary<String, Class_Type_ID> blob = new Dictionary<String, Class_Type_ID>();
            if (id <= 0)
            {
                id = db.find(table, ref spectrumname);
                id = Math.Max(id, GetMaxIDFromSpectrum());
            }
            
            data.Add("name",spectrumname);
            data.Add("id", id.ToString());
            blob.Add("spec", spec);
            db.ReplaceIntoBlob(table,ref data,ref blob);
            return id;
        }

        public void DeleteSpectrum(int id)
        {
                string table = "spectrum";
            db.Delete(table,"id",id.ToString());
        }
    }
}
