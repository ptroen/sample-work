﻿using System.IO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;


class Spectrum  : Class_Type_ID
{
   public Wave[] spectra;
  public    ViewSpectrum view;
    public Spectrum() { spectra = new Wave[16];
    for (int i = 0; i < 16;i++ )
    {
        spectra[i] = new Wave();
    }
        
        view = new ViewSpectrum();
    
    
    }

 public void ImportCSV()
 {
     StreamReader reader = new StreamReader("C:\\temp\\spec.csv");
         try
         {
             string header = reader.ReadLine();
             for (int j = 0; j < 16; j++)
             {
                 this.spectra[j].ImportCSV(ref reader);
             }
         }
         catch (Exception ex)
         {

             MessageBox.Show(ex.Message, ex.Message,
             MessageBoxButtons.OK, MessageBoxIcon.Error);
         }
    
 }
 public void ExportCSV()
 {
     StreamWriter writer = new StreamWriter("C:\\temp\\spec.csv");
     this.spectra[0].ExportCSVHeader(ref writer);
       for(int i=0;i<16;i++)
       {
           this.spectra[i].ExportCSV(ref writer);     
       }
     
 }

    public void Clear(int Temperature_In_K,short intensity )
    {
      int state = GetTemperatureState(Temperature_In_K);
     
        this.spectra[state].SetAllWavesConsistent(intensity);
         
    }
  public int GetMinTempState(int state){
        switch(state)
        {
            case 0: return 0;
            case 1: return 16;
               case 2: return 32;
            case 3: return 64;
           case 4: return 128;
            case 5: return 256;
               case 6: return 512;
            case 7: return 1024;
            case 8: return 2048;
            case 9: return 4096;
               case 10: return 8192;
            case 11: return 16384;
           case 12: return 32768;
            case 13: return 65536;
               case 14: return 131072;
            default: return 262144 ;
        }
    }

    public int GetTemperatureState(int tempearture_in_K){
        if(tempearture_in_K<0) return 0;
        else if(tempearture_in_K>=0 && tempearture_in_K<16) return 0;
        else if(tempearture_in_K>=16 && tempearture_in_K<32) return 1;
        else if(tempearture_in_K>=32 && tempearture_in_K<64) return 2;
        else if(tempearture_in_K>=64 && tempearture_in_K<128) return 3;
        else if(tempearture_in_K>=128 && tempearture_in_K<256) return 4;
        else if(tempearture_in_K>=256 && tempearture_in_K<512) return 5;
        else if(tempearture_in_K>=512 && tempearture_in_K<1024) return 6;
        else if(tempearture_in_K>=1024 && tempearture_in_K<2048) return 7;
        else if(tempearture_in_K>=2048 && tempearture_in_K<4096) return 8;
        else if(tempearture_in_K>=4096 && tempearture_in_K<8192) return 9;
        else if(tempearture_in_K>=8192 && tempearture_in_K<16384) return 10;
        else if(tempearture_in_K>=16384&& tempearture_in_K<32768) return 11;
        else if(tempearture_in_K>=32768 && tempearture_in_K<65536) return 12;
        else if(tempearture_in_K>=65536 && tempearture_in_K<131072) return 13;
        else if(tempearture_in_K>=131072 && tempearture_in_K<262144) return 14;
        else if(tempearture_in_K>=262144 && tempearture_in_K<524288) return 15;
        else  return 15;
    }
    public override int getsize()
    {
        return Marshal.SizeOf(this);
    }
    public override void ToByte(ref byte[] data)
    {
        System.Buffer.BlockCopy(spectra, 0, data, 0, getsize());
       }

       public override void SetFromByte(ref  byte[] data)
       {
        System.Buffer.BlockCopy(data, 0, spectra, 0, getsize());
        }

       public  void DrawSpectraColorSample(ref System.Drawing.Graphics g, 
                                                                                    int in_nm, 
                                                                                    int temperature_in_K, 
                                                                                    short intensity, 
                                                                                    int width, 
                                                                                    int height)
       {
           Spectrum samplespectrum=new Spectrum();
           int tempstate= samplespectrum.GetTemperatureState(temperature_in_K);
           samplespectrum.spectra[tempstate].SetWave(in_nm,intensity);
           // extract the sample color here lots of errors refactoring.....
           System.Drawing.Color rgb= samplespectrum.GetSpectraColor(temperature_in_K);
            DrawWavelength(ref g, ref rgb,0,0, width, height);

       }
       public System.Drawing.Color DrawSpectrum(ref System.Drawing.Graphics g, int passed_in_temperature_in_K)
        {
            
            // resultant color
            System.Drawing.Color rgb = GetSpectraColor(passed_in_temperature_in_K);
            int getindex=GetTemperatureState(passed_in_temperature_in_K);
           this.spectra[getindex].DrawSpectrum(ref g);

            return rgb;
        }
       protected void DrawWavelength(ref System.Drawing.Graphics g, ref System.Drawing.Color rgb,int start_x,int start_y, int width, int height)
       {
           System.Drawing.SolidBrush br = new SolidBrush(rgb);
           g.FillRectangle(br, start_x, start_y, width, height);
       
       }
       private System.Drawing.Color GetSpectraColor( int temperature_in_K)
    {
  
               System.Drawing.Color mincol=new System.Drawing.Color();
               System.Drawing.Color maxcol = new System.Drawing.Color();
               System.Drawing.Color rescol = new System.Drawing.Color();
        ViewSpectrum view = new ViewSpectrum();
        int numerator, denominator;
        int min_state_temp, max_state_temp;
        int min_state = this.GetTemperatureState(temperature_in_K);
        int max_state = Math.Min(min_state + 1, 15);
        min_state_temp = GetMinTempState(min_state);
        max_state_temp = GetMinTempState(max_state);    
        denominator = max_state_temp - min_state_temp + 1;
        numerator = temperature_in_K + 1 - min_state_temp;
          mincol= this.spectra[min_state].GetSpectraColor();
          maxcol = this.spectra[max_state].GetSpectraColor();
           int alpha,red,green,blue;
           alpha = ((mincol.A*numerator) + maxcol.A*(denominator-numerator)) / denominator;
           red = ((mincol.R * numerator) + maxcol.R * (denominator - numerator)) / denominator;
           green = ((mincol.G * numerator) + maxcol.G * (denominator - numerator)) / denominator;
           blue = ((mincol.B * numerator) + maxcol.B * (denominator - numerator)) / denominator;
           rescol = System.Drawing.Color.FromArgb(alpha, red, green, blue);
           return rescol;
    }

    public short GetSpectrum(int in_nm,int temperaturestate){
        return this.spectra[temperaturestate].GetWave(in_nm);
    }

    public void SetSpectrum(short wavelength_intensity,int in_nm, int temperaturestate)
    {
        this.spectra[temperaturestate].SetWave(in_nm, wavelength_intensity);
    }
};