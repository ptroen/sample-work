<?php
 require_once('sqllite_include.php');

?>

 <?php // stylesheet headers ?>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.8.23/themes/base/jquery-ui.css" type="text/css" media="all" />
<link rel="stylesheet" href="http://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css" type="text/css" media="all" />


<?php // javasript headers ?>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.pack.js"></script></script>
 <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js" type="text/javascript"></script>
  <script src="http://code.jquery.com/ui/1.8.23/jquery-ui.min.js" type="text/javascript"></script>

<?php // local headers ?>
  <script type="text/javascript" src="library.js"></script>


<?php // for running the tabs ?>
 <script>
 var entity_id;
 var animation_id;
 var faces_id;

window.setInterval(yourfunction, 1000);

function yourfunction() { $("#result").text('test'); }
	$(function() {

		$( "#tabs" ).tabs();
	});
	</script>



 <H1> Entity Editor </H1>
<div id="tabs">
	<ul>
                <li><a href="#selectEntity">Select Entity</a></li>

		<li><a href="#animate">Animation</a></li>
		<li><a href="#animate_frame">Frame</a></li>
		<li><a href="#faces">Faces</a></li>
		<li><a href="#stats">Entity Statistics</a></li>
		<li><a href="#script">Entity Scripting</a></li>
	</ul>
	<div id="selectEntity">
	<script>   
       
      	function updateFaces(id){
          window.faces_id=id;
          
          	// Source is the DataSource
		$("#selectanimation").autocomplete({
			source: "_selectFaces.php?pk_id="+id+"&",
                        select: function(event,ui){
		            updateframe(ui.item.id);
                         }
               });

        }



       	function updateframe(id){

          window.animation_id=id;

          	// Source is the DataSource
		$("#selectframe").autocomplete({
			source: "_selectFrame.php?pk_id="+id+"&",
                        select: function(event,ui){
		            updateFaces(ui.item.id);
                         }
               });
        }




	function updateanimation(id){
          window.entity_id=id;
          // alert("_selectAnimation.php?pk_id="+id+"&term="+id);

             	// Source is the DataSource
		$("#selectanimation").autocomplete({
			source: "_selectAnimation.php?pk_id="+id+"&",
                        select: function(event,ui){
		            updateframe(ui.item.id);
                         }
               });

        }


		// http://www.simonbattersby.com/blog/jquery-ui-autocomplete-with-a-remote-database-and-php/

         $(function() {

                        //
		// Source is the DataSource
		$("#selectentity").autocomplete({
			source: "_selectentity.php",
                        select: function(event,ui){
		             updateanimation(ui.item.id);
                         }
               });

     });

	</script>


	

	<input id="selectentity">





                <br/>
  Name to Insert: <input type="text" id="entityname"   name="entityname" onchange="
  setcookie('Entity','name',this.value)
  ">
                  <button type="button" id="+Entity">+</button>
                  <button type="button" id="-Entity">-</button>

	</div>

	<div id="animate">
           	<input id="selectanimation">
	</div>
	<div id="animate_frame">
            	<input id="selectframe">
	</div>
	
		<div id="faces">
	          <input id="selectfaces">
	<canvas id="canvas1" width="256", height="256"></canvas>
	<script type="text/javascript">

	function setPixel(imageData, x, y, r, g, b, a) {
            index = (x + y * imageData.width) * 4;
            imageData.data[index+0] = r;
            imageData.data[index+1] = g;
            imageData.data[index+2] = b;
            imageData.data[index+3] = a;
            }

          element = document.getElementById("canvas1");
          c = element.getContext("2d");
      
          // read the width and height of the canvas
          width = element.width;
          height = element.height;
      
          // create a new batch of pixels with the same
          // dimensions as the image:
          imageData = c.createImageData(width, height);

         </script>
        <?php
          // http://beej.us/blog/data/html5s-canvas-2-pixel/




        ?>
	</div>

	<div id="stats">

	</div>
	<div id="script">

	</div>
</div>

  
<div id="result">

</div>